<?php session_start();
  if(array_key_exists('admin', $_SESSION) && $_SESSION["admin"])
  {
    header('Location: planning.php');
  }
?>
<html>
	<head>
	  <title>Connexion Admin - OSI</title>
	  <meta charset="utf-8">
	  <!-- <meta name="viewport" content="width=device-width, initial-scale=1.0">  -->
	  <link rel="stylesheet" type="text/css" href="css/materialize.min.css">
	  <link rel="icon" type="icon" href="../appAndroid/OlympiadeSI/app/src/main/res/drawable/launcher_icon.png">
	  <link rel="stylesheet" type="text/css" href="css/materialize.css">
	      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	      <link href="css/style.css" rel="stylesheet">
	</head>

	<body>
  	  <?php include 'nav.php'; ?>
	  <div id="wrap">
			<div id="main">

			  <div id="zone-centrale">
			    <div id="zone-button">
		        <div class="log input-field col s6">
		          <i class="material-icons prefix">account_circle</i>
		          <input name="login" id="login" type="text" class="validate">
		          <label for="login">Login</label>
		        </div>
		        <div class="pass input-field col s6">
		            <i class="material-icons prefix">lock_circle</i>
		            <input name="password" id="password" type="password" class="validate">
		            <label for="password">Mot de passe</label>
		        </div>
						<a id="send" class="waves-effect waves-light btn"><i class="material-icons right">cloud</i>Connexion</a>
			    </div>
			    <div id="resultat">
			    </div>
			  </div>
			</div> <!-- end main -->
		</div> <!-- end wrap -->

	  <?php include 'footer.php';?>
  	  <script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
	  <script type="text/javascript" src="js/materialize.min.js"></script>
	  <script>
	        $(document).ready(function(){
	        $('select').material_select();
	            });
	        $("#send").click(function(){
			    $.ajax({
			       url : '/projet/siteWeb/traitement/traitementLogin.php',
			       type : 'POST', // Le type de la requête HTTP, ici devenu POST
			       data : 'login=' + document.getElementById('login').value + '&password=' + document.getElementById('password').value,
			       dataType : 'html',
			       success : function(code_html, statut){
			       		console.log(code_html);
			       		if(code_html.length === 0)
			       		{
							window.location.replace("/projet/siteWeb/planning.php");
			       		}
			       		else
			       		{
			       			document.getElementById('resultat').innerHTML = '<p style="color:red;">Login ou mot de passe invalide</p>';
			       		}
			       },
			    });
			});

	  </script>
	</body>
</html>
