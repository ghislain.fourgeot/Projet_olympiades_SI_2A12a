<?php
include "traitementAndroid/ConnectionBD.php";
if(array_key_exists('action', $_POST) && $_POST['action'] == 'ajouter'){

  $sql = 'Select max(id) as id from PERSONNE';
  $result = $file_db->prepare($sql);
  $result->execute();
  $res = $result->fetch(PDO::FETCH_ASSOC);
  $res['id']+= 1;

  $sql = 'INSERT INTO PERSONNE VALUES(:id, :nom, :prenom)';
  $stmt = $file_db->prepare($sql);
  $stmt->bindValue(':id', $res['id']);
  $stmt->bindParam(':nom', $_POST['nomEtudiant']);
  $stmt->bindParam(':prenom', $_POST['prenomEtudiant']);
  try
  {
    $stmt->execute();
    $sql = 'INSERT INTO ETUDIANT VALUES(:id, :lycee, :formation, :idP)';
    $stmt = $file_db->prepare($sql);
    $stmt->bindValue(':id', $res['id']);
    $stmt->bindParam(':lycee', $_POST['lycee']);
    $stmt->bindParam(':formation', $_POST['formation']);
    $stmt->bindParam(':idP', $_POST['idP']);
    $stmt->execute();
    echo "Ajout réussi";
  }
  catch(Exception $e)
  {
    echo "Ajout ratée";
  }


}
elseif(array_key_exists('action', $_POST) && $_POST['action'] == 'recuperation'){
  $sql = 'Select * from ETUDIANT natural join PERSONNE where idE= id and idE= :id';
  $stmt = $file_db->prepare($sql);
  $stmt->bindValue(':id', $_POST['idE']);
  $stmt->execute();
  $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
  print_r(json_encode($result));
}
elseif(array_key_exists('action', $_POST) && $_POST['action'] == 'modifier'){

  $sql2 = 'UPDATE PERSONNE SET nom=:nom, prenom=:prenom where id =:id';
  $stmt2 = $file_db->prepare($sql2);
  $stmt2->bindValue(':id', $_POST['idE']);
  $stmt2->bindParam(':nom', $_POST['nomEtudiant']);
  $stmt2->bindParam(':prenom', $_POST['prenomEtudiant']);
  try
  {
    $stmt2->execute();
    $sql = 'UPDATE ETUDIANT SET lycee=:lycee, formation=:formation, idP = :idP where idE =:id';
    $stmt = $file_db->prepare($sql);
    $stmt->bindValue(':id', $_POST['idE']);
    $stmt->bindParam(':idP', $_POST['idP']);
    $stmt->bindParam(':lycee', $_POST['lycee']);
    $stmt->bindParam(':formation', $_POST['formation']);
    $stmt->execute();
    echo "Modification réussi";
  }
  catch(Exception $e)
  {
    echo "Modification ratée";
  }



}
elseif(array_key_exists('action', $_POST) && $_POST['action'] == 'supprimer'){

  $sql = 'DELETE FROM ETUDIANT where idE=:id';
  $stmt = $file_db->prepare($sql);
  $stmt->bindValue(':id', $_POST['idE']);
  try
  {
    $stmt->execute();
    $sql2 = 'DELETE FROM PERSONNE where id=:id';
    $stmt2 = $file_db->prepare($sql2);
    $stmt2->bindValue(':id', $_POST['idE']);
    $stmt->execute();
    echo "Suppression réussi";
  }
  catch(Exception $e)
  {
    echo "Suppression ratée";
  }
}
elseif(array_key_exists('action', $_POST) && $_POST['action'] == 'recupProjets'){
  $sth = $file_db->prepare("Select * from PROJET");
  $sth->execute();
  $result = $sth->fetchAll(\PDO::FETCH_ASSOC);
  print_r(json_encode($result));
}
else
{
  $sth = $file_db->prepare("Select * from ETUDIANT natural join PERSONNE natural join PROJET where idE=id and idE>0");
  $sth->execute();
  $result = $sth->fetchAll(\PDO::FETCH_ASSOC);
  print_r(json_encode($result));
}

$file_db = null;
?>
