<?php
include "traitementAndroid/ConnectionBD.php";
if(array_key_exists('action', $_POST) && $_POST['action'] == 'ajouter'){

  $sql = 'Select max(idP) as idP from PROJET';
  $result = $file_db->prepare($sql);
  $result->execute();
  $res = $result->fetch(\PDO::FETCH_ASSOC);
  $res['idP']+= 1;

  $sql = 'INSERT INTO PROJET VALUES(:id, :nom, :description, :image, :location)';
  $stmt = $file_db->prepare($sql);
  $stmt->bindValue(':id', $res['idP']);
  $stmt->bindParam(':nom', $_POST['nomProjet']);
  $stmt->bindParam(':description', $_POST['description']);
  $stmt->bindParam(':image', $_POST['image']);
  $stmt->bindParam(':location', $_POST['emplacement']);
  try
  {
    $stmt->execute();
    echo "Ajout réussi";
  }
  catch(Exception $e)
  {
    echo "Ajout ratée";
  }
}
elseif(array_key_exists('action', $_POST) && $_POST['action'] == 'recuperation'){
  $sql = 'Select * from PROJET where idP= :id';
  $stmt = $file_db->prepare($sql);
  $stmt->bindValue(':id', $_POST['idP']);
  $stmt->execute();
  $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
  print_r(json_encode($result));
}
elseif(array_key_exists('action', $_POST) && $_POST['action'] == 'modifier'){
  $sql = 'UPDATE PROJET SET nomP=:nom , description=:description , image=:image , emplacement=:location   where idP=:id';
  $stmt = $file_db->prepare($sql);
  $stmt->bindValue(':id', $_POST['idP']);
  $stmt->bindParam(':nom', $_POST['nomProjet']);
  $stmt->bindParam(':description', $_POST['description']);
  $stmt->bindParam(':image', $_POST['image']);
  $stmt->bindParam(':location', $_POST['emplacement']);
  try
  {
    $stmt->execute();
    echo "Modification réussi";
  }
  catch(Exception $e)
  {
    echo "Modification ratée";
  }

}
elseif(array_key_exists('action', $_POST) && $_POST['action'] == 'supprimer'){

  $sql2= 'Select idE from ETUDIANT where idP= :id';
  $stmt2 = $file_db->prepare($sql2);
  $stmt2->bindValue(':id', $_POST['idP']);
  $stmt2 ->execute();
  $res2 = $stmt2->fetchAll(\PDO::FETCH_ASSOC);
  if (empty($res2)==FALSE){
    foreach($res2 as $res2){
      $sql2='UPDATE ETUDIANT SET idP = :idP where idE =:idE';
      $stmt2 = $file_db->prepare($sql2);
      $stmt2->bindValue(':idE', $res2['idE']);
      $stmt2->bindValue(':idP', 0);
      $stmt2 ->execute();
    }
  }


  $sql2= 'Select idJ from NOTES where idP= :id';
  $stmt2 = $file_db->prepare($sql2);
  $stmt2->bindValue(':id', $_POST['idP']);
  $stmt2 ->execute();
  $res2 = $stmt2->fetchAll(\PDO::FETCH_ASSOC);
  if (empty($res2)==FALSE){
    foreach($res2 as $res2){
      $sql2='DELETE FROM NOTES where idP =:idP and idJ=:idJ';
      $stmt2 = $file_db->prepare($sql2);
      $stmt2->bindValue(':idJ', $res2['idJ']);
      $stmt2->bindValue(':idP', $_POST['idP']);
      $stmt2 ->execute();
    }
  }

  $sql2= 'Select idJ from JUGE where idP= :id';
  $stmt2 = $file_db->prepare($sql2);
  $stmt2->bindValue(':id', $_POST['idP']);
  $stmt2 ->execute();
  $res2 = $stmt2->fetchAll(\PDO::FETCH_ASSOC);
  if (empty($res2)==FALSE){
    foreach($res2 as $res2){
      $sql2='DELETE FROM JUGE where idP =:idP and idJ=:idJ';
      $stmt2 = $file_db->prepare($sql2);
      $stmt2->bindValue(':idJ', $res2['idJ']);
      $stmt2->bindValue(':idP', $_POST['idP']);
      $stmt2 ->execute();
    }
  }


  $sql = 'DELETE FROM PROJET where idP=:id';
  $stmt = $file_db->prepare($sql);
  $stmt->bindValue(':id', $_POST['idP']);
  try
  {
    $stmt->execute();
    echo "Suppression réussi";
  }
  catch(Exception $e)
  {
    echo "Suppression ratée";
  }
}

else{
  $sth = $file_db->prepare("Select * from PROJET where idP>0");
  $sth->execute();
  $result = $sth->fetchAll(\PDO::FETCH_ASSOC);
  print_r(json_encode($result));
}

$file_db = null;
?>
