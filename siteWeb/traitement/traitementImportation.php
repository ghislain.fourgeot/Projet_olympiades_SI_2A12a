<?php
include "traitementAndroid/ConnectionBD.php";
print_r(file_get_contents($_FILES["file"]["tmp_name"]));
if(array_key_exists('action', $_POST) && $_POST['table'] == 'ETUDIANT')
{
  if (($handle = fopen($_FILES["file"]["tmp_name"], "r")) !== FALSE) {
    while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
      $sql = 'select id from PERSONNE where id = :id';
      $stmt = $file_db->prepare($sql);
      $stmt->bindValue(':id', $data[0]);
      $stmt->execute();
      $res = $stmt->fetchAll(\PDO::FETCH_ASSOC);
      if (empty($res)){
        $sql = 'INSERT INTO PERSONNE VALUES(:id, :nom, :prenom)';
        $stmt = $file_db->prepare($sql);
        $stmt->bindValue(':id', $data[0]);
        $stmt->bindParam(':nom', $data[1]);
        $stmt->bindParam(':prenom', $data[2]);
        $stmt->execute();
      }
      $sql ='select id from PERSONNE where id = :id';
      $stmt = $file_db->prepare($sql);
      $stmt->bindValue(':id', $data[0]);
      $stmt->execute();
      $res = $stmt->fetchAll(\PDO::FETCH_ASSOC);
      if (empty($res)){
        $sql2 = 'INSERT INTO ETUDIANT VALUES(:idE, :lycee, :formation, :idP)';
        $stmt2 = $file_db->prepare($sql2);
        $stmt2->bindValue(':idE', $data[0]);
        $stmt2->bindParam(':lycee', $data[3]);
        $stmt2->bindParam(':formation', $data[4]);
        $sql ='select idP from PROJET where idP = :id';
        $stmt = $file_db->prepare($sql);
        $stmt->bindValue(':id', $data[0]);
        $stmt->execute();
        $res = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        if (empty($res)==FALSE){
          $stmt2->bindParam(':idP', $data[5]);
        }
        else{
          $stmt2->bindParam(':idP', 0);
        }

        $stmt2->execute();
      }
    }

    fclose($handle);
  }
}
if(array_key_exists('action', $_POST) && $_POST['table'] == 'MEMBRE_DU_JURY')
{
  if (($handle = fopen($_FILES["file"]["tmp_name"], "r")) !== FALSE) {
    while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
      $sql = 'select id from PERSONNE where id = :id';
      $stmt = $file_db->prepare($sql);
      $stmt->bindValue(':id', $data[0]);
      $stmt->execute();
      $res = $stmt->fetchAll(\PDO::FETCH_ASSOC);
      if (empty($res)){
        $sql = 'INSERT INTO PERSONNE VALUES(:id, :nom, :prenom)';
        $stmt = $file_db->prepare($sql);
        $stmt->bindValue(':id', $data[0]);
        $stmt->bindParam(':nom', $data[1]);
        $stmt->bindParam(':prenom', $data[2]);
        $stmt->execute();
      }
      $sql ='select id from PERSONNE where id = :id';
      $stmt = $file_db->prepare($sql);
      $stmt->bindValue(':id', $data[0]);
      $stmt->execute();
      $res = $stmt->fetchAll(\PDO::FETCH_ASSOC);
      if (empty($res)){
        $sql2 = 'INSERT INTO MEMBRE_DU_JURY VALUES(:idM, :origine, :idJ)';
        $stmt2 = $file_db->prepare($sql2);
        $stmt2->bindValue(':idM', $data[0]);
        $stmt2->bindParam(':origine', $data[3]);
        $sql ='select idP from JURY where idJ = :id';
        $stmt = $file_db->prepare($sql);
        $stmt->bindValue(':id', $data[0]);
        $stmt->execute();
        $res = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        if (empty($res)==FALSE){
          $stmt2->bindParam(':idJ', $data[4]);
        }
        else{
          $stmt2->bindParam(':idJ', 0);
        }
        $stmt2->execute();
      }
    }
    fclose($handle);
  }
}
if(array_key_exists('action', $_POST) && $_POST['table'] == 'PROJET')
{
  if (($handle = fopen($_FILES["file"]["tmp_name"], "r")) !== FALSE) {
    while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
      $sql = 'select idP from PROJET where idP = :id';
      $stmt = $file_db->prepare($sql);
      $stmt->bindValue(':id', $data[0]);
      $stmt->execute();
      $res = $stmt->fetchAll(\PDO::FETCH_ASSOC);
      if (empty($res)){
        $sql = 'INSERT INTO PROJET VALUES(:idP, :nomP, :description, :image, :emplacement)';
        $stmt = $file_db->prepare($sql);
        $stmt->bindValue(':idP', $data[0]);
        $stmt->bindParam(':nomP', $data[1]);
        $stmt->bindParam(':description', $data[2]);
        $stmt->bindParam(':image', $data[3]);
        $stmt->bindParam(':emplacement', $data[4]);
        $stmt->execute();
      }
    }
    fclose($handle);
  }
}
if(array_key_exists('action', $_POST) && $_POST['table'] == 'CRENEAU')
{
  if (($handle = fopen($_FILES["file"]["tmp_name"], "r")) !== FALSE) {
    while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
      $sql = 'select idC from CRENEAU where idC = :id';
      $stmt = $file_db->prepare($sql);
      $stmt->bindValue(':id', $data[0]);
      $stmt->execute();
      $res = $stmt->fetchAll(\PDO::FETCH_ASSOC);
      if (empty($res)){
        $sql = 'INSERT INTO CRENEAU VALUES(:idC, :hdebut, :hfin)';
        $stmt = $file_db->prepare($sql);
        $stmt->bindValue(':idC', $data[0]);
        $stmt->bindParam(':hdebut', $data[1]);
        $stmt->bindParam(':hfin', $data[2]);
        $stmt->execute();
      }
    }
    fclose($handle);
  }
}
header('Location: ../gestion.php');
$file_db = null;
?>
