<?php
include "traitementAndroid/ConnectionBD.php";
if(array_key_exists('action', $_POST) && $_POST['action'] == 'ajouter'){

  $sql = 'Select max(id) as id from PERSONNE';
  $result = $file_db->prepare($sql);
  $result->execute();
  $res = $result->fetch(PDO::FETCH_ASSOC);
  $res['id']+= 1;

  $sql = 'INSERT INTO PERSONNE VALUES(:id, :nom, :prenom)';
  $stmt = $file_db->prepare($sql);
  $stmt->bindValue(':id', $res['id']);
  $stmt->bindParam(':nom', $_POST['nomJure']);
  $stmt->bindParam(':prenom', $_POST['prenomJure']);
  try
  {
    $stmt->execute();
    echo "Ajout réussi";
    $sql = 'INSERT INTO MEMBRE_DU_JURY VALUES(:id, :origine, :idJ)';
    $stmt = $file_db->prepare($sql);
    $stmt->bindValue(':id', $res['id']);
    $stmt->bindParam(':origine', $_POST['origine']);
    $stmt->bindParam(':idJ', $_POST['idJ']);
    $stmt->execute();
  }
  catch(Exception $e)
  {
    echo "Ajout ratée";
  }


}
elseif(array_key_exists('action', $_POST) && $_POST['action'] == 'recuperation'){
  $sql = 'Select * from MEMBRE_DU_JURY natural join PERSONNE where idM= id and idM= :id';
  $stmt = $file_db->prepare($sql);
  $stmt->bindValue(':id', $_POST['idM']);
  $stmt->execute();
  $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
  print_r(json_encode($result));
}
elseif(array_key_exists('action', $_POST) && $_POST['action'] == 'recupJury'){
  $sth = $file_db->prepare("Select * from JURY where idJ >= 0");
  $sth->execute();
  $result = $sth->fetchAll(\PDO::FETCH_ASSOC);
  print_r(json_encode($result));
}
elseif(array_key_exists('action', $_POST) && $_POST['action'] == 'modifier'){

  $sql2 = 'UPDATE PERSONNE SET nom=:nom, prenom=:prenom where id =:id';
  $stmt2 = $file_db->prepare($sql2);
  $stmt2->bindValue(':id', $_POST['idM']);
  $stmt2->bindParam(':nom', $_POST['nomJure']);
  $stmt2->bindParam(':prenom', $_POST['prenomJure']);
  try
  {
    $stmt2->execute();
    $sql = 'UPDATE MEMBRE_DU_JURY SET origine=:origine, idJ = :idJ where idM =:id';
    $stmt = $file_db->prepare($sql);
    $stmt->bindValue(':id', $_POST['idM']);
    $stmt->bindParam(':idJ', $_POST['idJ']);
    $stmt->bindParam(':origine', $_POST['origine']);
    $stmt->execute();
    echo "Modification réussi";
  }
  catch(Exception $e)
  {
    echo "Modification ratée";
  }



}
elseif(array_key_exists('action', $_POST) && $_POST['action'] == 'supprimer'){

  $sql = 'DELETE FROM MEMBRE_DU_JURY where idM=:id';
  $stmt = $file_db->prepare($sql);
  $stmt->bindValue(':id', $_POST['id']);
  try
  {
    $stmt->execute();
    $sql2 = 'DELETE FROM PERSONNE where id=:id';
    $stmt2 = $file_db->prepare($sql2);
    $stmt2->bindValue(':id', $_POST['id']);
    $stmt2->execute();
    echo "Suppression réussi";
  }
  catch(Exception $e)
  {
    echo "Suppression ratée";
  }
}
else
{
  $sth = $file_db->prepare("Select * from MEMBRE_DU_JURY natural join PERSONNE where idM=id and idM>0");
  $sth->execute();
  $result = $sth->fetchAll(\PDO::FETCH_ASSOC);
  for ($i = 0; $i < count($result); $i++)
  {
    $result[$i]['nom'] = utf8_encode($result[$i]['nom']);
    $result[$i]['prenom'] = utf8_encode($result[$i]['prenom']);
    $result[$i]['origine'] = utf8_encode($result[$i]['origine']);
  }
  print_r(json_encode($result));
}

$file_db = null;
?>
