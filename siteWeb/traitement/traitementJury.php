<?php
include "traitementAndroid/ConnectionBD.php";
if(array_key_exists('action', $_POST) && $_POST['action'] == 'ajouter'){

  $sql = 'Select max(idJ) as idJ from JURY';
  $result = $file_db->prepare($sql);
  $result->execute();
  $res = $result->fetch(PDO::FETCH_ASSOC);
  $res['idJ']+= 1;

  $sql = 'INSERT INTO JURY VALUES(:id, :identifiant, :mdp)';
  $hashed =  hash('sha224', $_POST['mdp']);
  $stmt = $file_db->prepare($sql);
  $stmt->bindValue(':id', $res['idJ']);
  $stmt->bindParam(':identifiant', $_POST['identifiant']);
  $stmt->bindParam(':mdp', $hashed);
  try
  {
    $stmt->execute();
    echo "Ajout réussi";
  }
  catch(Exception $e)
  {
    echo "Ajout ratée";
  }
}
elseif(array_key_exists('action', $_POST) && $_POST['action'] == 'recuperation'){
  $sql = 'Select * from JURY where idJ= :id';
  $stmt = $file_db->prepare($sql);
  $stmt->bindValue(':id', $_POST['idJ']);
  $stmt->execute();
  $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
  print_r(json_encode($result));
}
elseif(array_key_exists('action', $_POST) && $_POST['action'] == 'modifier'){

  $sql = 'UPDATE JURY SET identifiant=:identifiant, mot_de_passe = :mdp where idJ =:id';
  $stmt = $file_db->prepare($sql);
  $hashed =  hash('sha224', $_POST['mdp']);
  $stmt->bindValue(':id', $_POST['idJ']);
  $stmt->bindParam(':mdp', $hashed);
  $stmt->bindParam(':identifiant', $_POST['identifiant']);
  try
  {
    $stmt->execute();
    echo "Modification réussi";
  }
  catch(Exception $e)
  {
    echo "Modification ratée";
  }

}
elseif(array_key_exists('action', $_POST) && $_POST['action'] == 'supprimer'){

  $sql2= 'Select idM from MEMBRE_DU_JURY where idJ= :id';
  $stmt2 = $file_db->prepare($sql2);
  $stmt2->bindValue(':id', $_POST['idJ']);
  $stmt2 ->execute();
  $res2 = $stmt2->fetchAll(\PDO::FETCH_ASSOC);
  if (empty($res2)==FALSE){
    foreach($res2 as $res2){
      $sql2='UPDATE MEMBRE_DU_JURY SET idJ = :idJ where idM =:idM';
      $stmt2 = $file_db->prepare($sql2);
      $stmt2->bindValue(':idM', $res2['idM']);
      $stmt2->bindValue(':idJ', 0);
      $stmt2 ->execute();
    }
  }

  $sql2= 'Select idP from NOTES where idJ= :id';
  $stmt2 = $file_db->prepare($sql2);
  $stmt2->bindValue(':id', $_POST['idJ']);
  $stmt2 ->execute();
  $res2 = $stmt2->fetchAll(\PDO::FETCH_ASSOC);
  if (empty($res2)==FALSE){
    foreach($res2 as $res2){
      $sql2='DELETE FROM NOTES where idP =:idP and idJ=:idJ';
      $stmt2 = $file_db->prepare($sql2);
      $stmt2->bindValue(':idP', $res2['idP']);
      $stmt2->bindValue(':idJ', $_POST['idJ']);
      $stmt2 ->execute();
    }
  }


  $sql2= 'Select idP from JUGE where idJ= :id';
  $stmt2 = $file_db->prepare($sql2);
  $stmt2->bindValue(':id', $_POST['idJ']);
  $stmt2 ->execute();
  $res2 = $stmt2->fetchAll(\PDO::FETCH_ASSOC);
  if (empty($res2)==FALSE){
    foreach($res2 as $res2){
      $sql2='DELETE FROM JUGE where idP =:idP and idJ=:idJ';
      $stmt2 = $file_db->prepare($sql2);
      $stmt2->bindValue(':idP', $res2['idP']);
      $stmt2->bindValue(':idJ', $_POST['idJ']);
      $stmt2 ->execute();
    }
  }

  $sql = 'DELETE FROM JURY where idJ=:id';
  $stmt = $file_db->prepare($sql);
  $stmt->bindValue(':id', $_POST['idJ']);
  try
  {
    $stmt->execute();
    echo "Suppression réussi";
  }
  catch(Exception $e)
  {
    echo "Suppression ratée";
  }
}
else
{
  $sth = $file_db->prepare("Select * from JURY where idJ!=0");
  $sth->execute();
  $result = $sth->fetchAll(\PDO::FETCH_ASSOC);
  print_r(json_encode($result));
}

$file_db = null;
?>
