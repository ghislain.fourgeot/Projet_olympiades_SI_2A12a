<?php
include "traitementAndroid/ConnectionBD.php";

if(array_key_exists('action', $_POST) && $_POST['action'] == 'recuperation')
{
  $sql = 'Select nomP,hdebut,hfin,identifiant,etat,emplacement,lycee from PROJET natural join JUGE natural join CRENEAU natural join JURY natural join NOTES natural join ETUDIANT where idJ>0 group by idJ,idP order by hdebut,idJ';
  $stmt = $file_db->prepare($sql);
  $stmt->execute();
  $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
  print_r(json_encode($result));
}
elseif(array_key_exists('action', $_POST) && $_POST['action'] == 'jury')
{
  $sql = 'Select identifiant from JURY where idJ>0 order by idJ';
  $stmt = $file_db->prepare($sql);
  $stmt->execute();
  $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
  print_r(json_encode($result));
}
elseif(array_key_exists('action', $_POST) && $_POST['action'] == 'creneau')
{
  $sql = 'Select hdebut,hfin from CRENEAU order by hdebut';
  $stmt = $file_db->prepare($sql);
  $stmt->execute();
  $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
  print_r(json_encode($result));
}
elseif(array_key_exists('action', $_POST) && $_POST['action'] == 'generer')
{
  $sql = 'Select idP from PROJET where nomP=:nomP';
  $stmt = $file_db->prepare($sql);
  $stmt->bindValue(':nomP', $_POST['nomP']);
  $stmt->execute();
  $res = $stmt->fetch(\PDO::FETCH_ASSOC);
  $idP=$res['idP'];

  $sql = 'Select idJ from JURY where identifiant=:identifiant';
  $stmt = $file_db->prepare($sql);
  $stmt->bindValue(':identifiant', $_POST['identifiant']);
  $stmt->execute();
  $res = $stmt->fetch(\PDO::FETCH_ASSOC);
  $idJ=$res['idJ'];

  $sql = 'Select idC from CRENEAU where hdebut=:hdebut and hfin =:hfin';
  $stmt = $file_db->prepare($sql);
  $stmt->bindValue(':hdebut', $_POST['hdebut']);
  $stmt->bindValue(':hfin', $_POST['hfin']);
  $stmt->execute();
  $res = $stmt->fetch(\PDO::FETCH_ASSOC);
  $idC=$res['idC'];

  $sql = 'INSERT INTO JUGE VALUES(:idJ, :idP, :idC)';
  $stmt = $file_db->prepare($sql);
  $stmt->bindValue(':idJ', $idJ);
  $stmt->bindValue(':idP', $idP);
  $stmt->bindValue(':idC', $idC);
  try
  {
    $stmt->execute();
    $sql = 'INSERT INTO NOTES VALUES(:idJ,:idP,0,0,0,0,0,0,"attente")';
    $stmt = $file_db->prepare($sql);
    $stmt->bindValue(':idJ', $idJ);
    $stmt->bindValue(':idP', $idP);
    $stmt->execute();
    echo 1;
  }
  catch(Exception $e)
  {
    echo 0;
  }
}
elseif(array_key_exists('action', $_POST) && $_POST['action'] == 'supprimer')
{
  $sql = 'Select idP from PROJET where nomP=:nomP';
  $stmt = $file_db->prepare($sql);
  $stmt->bindValue(':nomP', $_POST['nomP']);
  $stmt->execute();
  $res = $stmt->fetch(\PDO::FETCH_ASSOC);
  $idP=$res['idP'];

  $sql = 'Select idJ from JURY where identifiant=:identifiant';
  $stmt = $file_db->prepare($sql);
  $stmt->bindValue(':identifiant', $_POST['identifiant']);
  $stmt->execute();
  $res = $stmt->fetch(\PDO::FETCH_ASSOC);
  $idJ=$res['idJ'];

  $sql = 'Select idC from CRENEAU where hdebut=:hdebut and hfin =:hfin';
  $stmt = $file_db->prepare($sql);
  $stmt->bindValue(':hdebut', $_POST['hdebut']);
  $stmt->bindValue(':hfin', $_POST['hfin']);
  $stmt->execute();
  $res = $stmt->fetch(\PDO::FETCH_ASSOC);
  $idC=$res['idC'];

  $sql = 'DELETE FROM JUGE WHERE idJ=:idJ and idP=:idP and idC=:idC';
  $stmt = $file_db->prepare($sql);
  $stmt->bindValue(':idJ', $idJ);
  $stmt->bindValue(':idP', $idP);
  $stmt->bindValue(':idC', $idC);
  try
  {
    $stmt->execute();
    $sql = 'DELETE FROM NOTES WHERE idJ=:idJ and idP=:idP';
    $stmt = $file_db->prepare($sql);
    $stmt->bindValue(':idJ', $idJ);
    $stmt->bindValue(':idP', $idP);
    $stmt->execute();
    echo 1;
  }
  catch(Exception $e)
  {
    echo 0;
  }
}
elseif(array_key_exists('action', $_POST) && $_POST['action'] == 'modifier')
{
  $sql = 'Select idP from PROJET where nomP=:nomP';
  $stmt = $file_db->prepare($sql);
  $stmt->bindValue(':nomP', $_POST['nomP']);
  $stmt->execute();
  $res = $stmt->fetch(\PDO::FETCH_ASSOC);
  $idP=$res['idP'];

  $sql = 'Select idJ from JURY where identifiant=:identifiant';
  $stmt = $file_db->prepare($sql);
  $stmt->bindValue(':identifiant', $_POST['identifianta']);
  $stmt->execute();
  $res = $stmt->fetch(\PDO::FETCH_ASSOC);
  $idJa=$res['idJ'];

  $sql = 'Select idC from CRENEAU where hdebut=:hdebut and hfin =:hfin';
  $stmt = $file_db->prepare($sql);
  $stmt->bindValue(':hdebut', $_POST['hdebuta']);
  $stmt->bindValue(':hfin', $_POST['hfina']);
  $stmt->execute();
  $res = $stmt->fetch(\PDO::FETCH_ASSOC);
  $idCa=$res['idC'];

  $sql = 'Select idJ from JURY where identifiant=:identifiant';
  $stmt = $file_db->prepare($sql);
  $stmt->bindValue(':identifiant', $_POST['identifiantd']);
  $stmt->execute();
  $res = $stmt->fetch(\PDO::FETCH_ASSOC);
  $idJd=$res['idJ'];

  $sql = 'Select idC from CRENEAU where hdebut=:hdebut and hfin =:hfin';
  $stmt = $file_db->prepare($sql);
  $stmt->bindValue(':hdebut', $_POST['hdebutd']);
  $stmt->bindValue(':hfin', $_POST['hfind']);
  $stmt->execute();
  $res = $stmt->fetch(\PDO::FETCH_ASSOC);
  $idCd=$res['idC'];

  $sql = 'UPDATE JUGE SET idC=:idCa, idJ=:idJa where idP=:idP and idC=:idCd and idJ=:idJd';
  $stmt = $file_db->prepare($sql);
  $stmt->bindValue(':idJa', $idJa);
  $stmt->bindValue(':idJd', $idJd);
  $stmt->bindValue(':idP', $idP);
  $stmt->bindValue(':idCa', $idCa);
  $stmt->bindValue(':idCd', $idCd);
  try
  {
    $stmt->execute();
    $sql = 'UPDATE NOTES SET idJ=:idJa WHERE idJ=:idJd and idP=:idP';
    $stmt = $file_db->prepare($sql);
    $stmt->bindValue(':idJa', $idJa);
    $stmt->bindValue(':idJd', $idJd);
    $stmt->bindValue(':idP', $idP);
    $stmt->execute();
    echo 1;
  }
  catch(Exception $e)
  {
    echo 0;
  }
}
elseif(array_key_exists('action', $_POST) && $_POST['action'] == 'Projetsdisponible')
{
  $sql = 'Select idJ from JURY where identifiant=:identifiant';
  $stmt = $file_db->prepare($sql);
  $stmt->bindValue(':identifiant', $_POST['identifiant']);
  $stmt->execute();
  $res = $stmt->fetch(\PDO::FETCH_ASSOC);
  $idJ=$res['idJ'];

  $sql = 'Select idC from CRENEAU where hdebut=:hdebut and hfin =:hfin';
  $stmt = $file_db->prepare($sql);
  $stmt->bindValue(':hdebut', $_POST['hdebut']);
  $stmt->bindValue(':hfin', $_POST['hfin']);
  $stmt->execute();
  $res = $stmt->fetch(\PDO::FETCH_ASSOC);
  $idC=$res['idC'];

  $sql="Select nomP,emplacement,lycee from PROJET natural join ETUDIANT where idP>0 and idP not in  (
    Select distinct idP from JUGE where idC=:idC or idJ=:idJ
    ) group by idP";
  $stmt = $file_db->prepare($sql);
  $stmt->bindValue(':idJ', $idJ);
  $stmt->bindValue(':idC', $idC);
  $stmt->execute();
  $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
  print_r(json_encode($result));
}
elseif(array_key_exists('action', $_POST) && $_POST['action'] == 'toutprojet')
{
  $sql="Select nomP,etat,identifiant,hdebut,hfin from PROJET natural join NOTES natural join JUGE natural join JURY natural join CRENEAU order by hdebut,idJ";
  $stmt = $file_db->prepare($sql);
  $stmt->execute();
  $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
  print_r(json_encode($result));
}
elseif(array_key_exists('action', $_POST) && $_POST['action'] == 'listeprojet')
{
  $sql="Select nomP from PROJET where idP>0";
  $stmt = $file_db->prepare($sql);
  $stmt->execute();
  $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
  print_r(json_encode($result));
}
$file_db = null;
?>
