<?php
include "ConnectionBD.php";

$result       =  0;
$notes        = array();
$creneau      = array();
$projet       = array();
$query = "SELECT idP,nomP,description,image,emplacement,originalite,prototype,Demarche_Scientifique,pluridisciplinarite,Maitrise_Scientifique
                  ,Communication,idC,hdebut,hfin,etat
                  from PROJET natural join JUGE natural join NOTES natural join CRENEAU where idJ=?";

$idj = $_POST["idJ"];

if(infoProj($file_db,$idj)){
    $result = 1;
}else{
    $result = 0;
}
print_r(json_encode(array("infoProjet" => $result,"creneau"=> $creneau, "notes"=> $notes, "projet"=> $projet)));

function infoProj($cnnn,$idJ){
    global $query, $creneau, $notes, $projet;
    $stmt = $cnnn->prepare($query);
    $stmt->bindParam(1, $idJ);
    $stmt->execute();
    while ($row = $stmt->fetch()){
        array_push($creneau,
                array(
                    "idC" => $row[11],
                    "hdebut" => $row[12],
                    "hfin" => $row[13]
                )
            );
        array_push($projet,
                array(
                    "idP" => $row[0],
                    "nomPro"=> $row[1],
                    "description" => $row[2],
                    "image" =>$row[3],
                    "emplacement"=>$row[4]
                )
        );
        array_push($notes,
                array(
                    "idJ" => $_POST["idJ"],
                    "idP" => $row[0],
                    "originalite"=> $row[5],
                    "prototype" => $row[6],
                    "Demarche_Scientifique" => $row[7],
                    "pluridisciplinarite" => $row[8],
                    "Maitrise_Scientifique" => $row[9],
                    "Communication" => $row[10],
                    "etat" => $row['etat']
                )
        );
    }
    $rowcount = $stmt->rowCount();
    return $rowcount;
}
