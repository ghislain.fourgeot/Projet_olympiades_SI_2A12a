<?php
include "ConnectionBD.php";

$idP    = $_POST["idP"];
$idJ    = $_POST["idJ"];
$result = 0;

$originalite           = $_POST["originalite"];
$prototype             = $_POST["prototype"];
$demarche_Scientifique = $_POST["demarche_Scientifique"];
$pluridisciplinarite   = $_POST["pluridisciplinarite"];
$Maitrise_Scientifique = $_POST["Maitrise_Scientifique"];
$Communication         = $_POST["Communication"];
$Etat                  = $_POST["Etat"];

$query  = "UPDATE NOTES
           SET originalite =:originalite,
              prototype =:prototype,
              Demarche_Scientifique =:Demarche_Scientifique,
              pluridisciplinarite =:pluridisciplinarite,
              Maitrise_Scientifique =:Maitrise_Scientifique,
              Communication =:Communication,
              etat =:etat
           where idP=:idp and idJ=:idJ";


if(update($file_db)){
    $result = 1;
}else{
    $result = 0;
}

print_r(json_encode(array("existsInBD"=>$result)));

function update($cnn){
    global $query, $idP, $idJ,$originalite,$prototype,$demarche_Scientifique,
           $pluridisciplinarite,$Maitrise_Scientifique,$Communication,$Etat;
    $stmt = $cnn->prepare($query);
    $stmt->bindParam(':originalite',$originalite);
    $stmt->bindParam(':prototype',$prototype);
    $stmt->bindParam(':Demarche_Scientifique',$demarche_Scientifique);
    $stmt->bindParam(':pluridisciplinarite',$pluridisciplinarite);
    $stmt->bindParam(':Maitrise_Scientifique',$Maitrise_Scientifique);
    $stmt->bindParam(':Communication',$Communication);
    $stmt->bindParam(':etat',$Etat);
    $stmt->bindParam(':idp',$idP);
    $stmt->bindParam(':idJ',$idJ);
    $stmt->execute();
    $rowCount = $stmt->rowCount();
    return $rowCount;
}
