La plupart des fichier de traitement php sont sur le meme patern et fonctionne par mot clé envoyé depuis la partie du site web qui lui est
destiné, cette description ne s'applique pas au fichier du dossier traitementAndroid.


ajouter :
permet d'ajouter un élément en base de données a partir des informations fournis par la fonction POST de php

recuperation:
permet de recupérer des informations en base de données spécifique a un élèment ou a une table en fonction des informations fournis
par la fonction post de php.

modifier:
permet de modifier un élèment déjà éxistant en base de donnée

supprimer:
permet de supprimer un élèment déjà éxistant en base de donnée

jury :
permet de récupérer la liste des identifiant de jury afin de générer le planning

creneau :
permet de récupérer la liste des Créneau pour générer le planning

generer :
permet de récupérer les informations liés au couple créneau,jury,projet pour générer le planning

Projetsdisponible :
permet de récupérer la liste des projets attribuable a une entrée spécifique du planning

toutprojet :
permet de récupérer la liste de tout les projets et de leur informations

listeprojet :
permet de récupérer uniquement les nom de tout les projets

sponsor :
permet de récupérer l'image d'un sponsor pour l'affichage

lycee_prix :
permet de récupérer les projets ayant reçus un prix

nombre_lycee :
permet de savoir le nombre de prix attribué a un lycée

recompense_existe :
permet de savoir si un projet donnée a déjà un prix ou non

assigner_prix :
permet d'assigner un prix a un projet

prix :
permet de récupérer la liste des prix
