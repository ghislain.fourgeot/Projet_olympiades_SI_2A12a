<?php
include "traitementAndroid/ConnectionBD.php";
if(array_key_exists('action', $_POST) && $_POST['action'] == 'recuperation')
{
  $sql = 'Select nomPrix,nomP,lycee,formation,AVG(originalite) as originalite,AVG(pluridisciplinarite)as pluridisciplinarite,AVG(Communication)as Communication,AVG(prototype) as prototype,AVG(Maitrise_Scientifique) as Maitrise_Scientifique,AVG(Demarche_Scientifique) as Demarche_Scientifique,AVG(originalite+pluridisciplinarite+Communication+prototype+Maitrise_Scientifique+Demarche_Scientifique)/6 as moyenne from PROJET natural join NOTES natural join ETUDIANT natural left outer join PRIX where idP>0 group by idP';
  $stmt = $file_db->prepare($sql);
  $stmt->execute();
  $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
  print_r(json_encode($result));
}
elseif(array_key_exists('action', $_POST) && $_POST['action'] == 'prix')
{
  $sql = 'Select idP,nomPrix,ordre,assigne,lycee,nomP,formation from PRIX natural join PROJET natural join ETUDIANT group by idP order by ordre';
  $stmt = $file_db->prepare($sql);
  $stmt->execute();
  $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
  print_r(json_encode($result));
}
elseif(array_key_exists('action', $_POST) && $_POST['action'] == 'actualiser_classement')
{
  $sql = 'Select idP,AVG(:critere) as moy from NOTES where idP in (Select idP from PROJET natural join ETUDIANT where idP not in (Select idP from PRIX) and lycee not in (Select lycee from PROJET natural join ETUDIANT natural join PRIX group by lycee having count(*)>=2)group by idP) group by idP';
  $stmt = $file_db->prepare($sql);
  $stmt->bindParam('critere', $_POST['critere']);
  $stmt->execute();
  $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
  $idP=0;
  $moy=0;
  if (empty($result)==FALSE){
    foreach($result as $res){
      if ($res['moy']>$moy){
        $moy=$res['moy'];
        $idP=$res['idP'];
      }
      }
      $sql2='UPDATE PRIX SET idP = :idP where critere =:critere';
      $stmt2 = $file_db->prepare($sql2);
      $stmt2->bindValue(':critere', $_POST['critere']);
      $stmt2->bindValue(':idP', $idP);
      try
      {
        $stmt2->execute();
        echo "actualisation réussi";
      }
      catch(Exception $e)
      {
        echo "actualisation ratée";
      }
  }

}

elseif(array_key_exists('action', $_POST) && $_POST['action'] == 'assigner_prix')
{
  $auto=True;
  $sql = 'Select idP,lycee from PROJET natural join ETUDIANT where nomP=:nomP group by nomP';
  $stmt = $file_db->prepare($sql);
  $stmt->bindValue(':nomP', $_POST['nomP']);
  $stmt->execute();
  $res = $stmt->fetch(\PDO::FETCH_ASSOC);
  $idP=$res['idP'];
  $lycee=$res['lycee'];

  if ($_POST['critere']=='general'){
    $sql = 'Select lycee as lycee from PRIX natural join PROJET natural join ETUDIANT where nomPrix= :general2';
    $stmt = $file_db->prepare($sql);
    $stmt->bindValue(':general2', 'general2');
    $stmt->execute();
    $res = $stmt->fetch(\PDO::FETCH_ASSOC);
    if($lycee==$res['lycee']){
      $auto=False;

      echo "assignation ratée car les 2 générals viennent du même lycée";
    }
  }
  if ($_POST['critere']=='general2'){
    $sql = 'Select lycee from PRIX natural join PROJET natural join ETUDIANT where nomPrix=:general';
    $stmt = $file_db->prepare($sql);
    $stmt->execute();
    $stmt->bindValue(':general', 'general');
    $stmt->execute();
    $res = $stmt->fetch(\PDO::FETCH_ASSOC);
    if($lycee==$res['lycee']){
      $auto=False;
      echo "assignation ratée car les 2 générals viennent du même lycée";
    }
  }
  $sql = 'Select lycee from PRIX natural join PROJET natural join ETUDIANT group by nomP';
  $stmt = $file_db->prepare($sql);
  $stmt->execute();
  $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
  $cpt=0;
  foreach($result as $res){
    if ($res['lycee']==$lycee){
      $cpt+=1;
    }
    if($cpt>=2){
      $auto=False;
      echo "assignation ratée car plus de 2 groupes viennent du meme lycée";
    }
  }



  if ($auto){
    $sql = 'UPDATE PRIX SET idP = :idP, assigne=True where nomPrix =:critere';
    $stmt = $file_db->prepare($sql);
    $stmt->bindValue(':critere', $_POST['critere']);
    $stmt->bindValue(':idP', $idP);
    try
    {
      $stmt->execute();
      echo "assignation réussi";
    }
    catch(Exception $e)
    {
      echo "assignation ratée";
    }
  }
}
elseif(array_key_exists('action', $_POST) && $_POST['action'] == 'recompense_existe')
{
  $sql = 'Select idP from PROJET where nomP=:nomP';
  $stmt = $file_db->prepare($sql);
  $stmt->bindValue(':nomP', $_POST['nomP']);
  $stmt->execute();
  $res=$stmt->fetch(\PDO::FETCH_ASSOC);
  $sql = 'Select idP from PRIX where idP=:idP';
  $stmt = $file_db->prepare($sql);
  $stmt->bindValue(':idP', $res['idP']);
  $stmt->execute();
  $res2 = $stmt->fetch(\PDO::FETCH_ASSOC);
  if(empty($res2)==FALSE){
    echo "erreur";
  }
}
elseif(array_key_exists('action', $_POST) && $_POST['action'] == 'nombre_lycee')
{
  $sql = 'Select lycee from PRIX natural join PROJET natural join ETUDIANT group by nomP';
  $stmt = $file_db->prepare($sql);
  $stmt->execute();
  $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
  $cpt=0;
  foreach($result as $res){
    if ($res['lycee']==$_POST['lycee']){
      $cpt+=1;
    }
  }
  echo $cpt;
}
elseif(array_key_exists('action', $_POST) && $_POST['action'] == 'suppression')
{
  $sql = 'UPDATE PRIX SET idP = 0 where nomPrix =:critere';
  $stmt = $file_db->prepare($sql);
  $stmt->bindValue(':critere', $_POST['critere']);
  try
  {
    $stmt->execute();
    echo "actualisation réussi";
  }
  catch(Exception $e)
  {
    echo "actualisation ratée";
  }
}
elseif(array_key_exists('action', $_POST) && $_POST['action'] == 'lycee_prix')
{
  $sql = 'Select lycee,nomP from PROJET natural join PRIX natural join ETUDIANT where lycee=:lycee group by nomP';
  $stmt = $file_db->prepare($sql);
  $stmt->bindValue(':lycee', $_POST['lycee']);
  $stmt->execute();
  $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
  print_r(json_encode($result));
}
elseif(array_key_exists('action', $_POST) && $_POST['action'] == 'sponsor')
{
  $sql = 'Select image from SPONSOR where type=:type';
  $stmt = $file_db->prepare($sql);
  $stmt->bindValue(':type', $_POST['type']);
  $stmt->execute();
  $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
  print_r(json_encode($result));
}
$file_db = null;
?>
