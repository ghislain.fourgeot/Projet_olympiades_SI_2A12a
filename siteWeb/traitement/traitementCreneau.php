<?php
include "traitementAndroid/ConnectionBD.php";
if(array_key_exists('action', $_POST) && $_POST['action'] == 'ajouter')
{
  $sql = 'Select max(idC) as idC from CRENEAU';
  $result = $file_db->prepare($sql);
  $result->execute();
  $res = $result->fetch(PDO::FETCH_ASSOC);
  $res['idC']+= 1;

  $sql = 'INSERT INTO CRENEAU VALUES(:idC, :debut, :fin)';
  $stmt = $file_db->prepare($sql);
  $stmt->bindValue(':idC', $res['idC']);
  $stmt->bindParam(':debut',$_POST['hdebut']);
  $stmt->bindParam(':fin',$_POST['hfin']);
  try
  {
    $stmt->execute();
    echo "Ajout réussi";
  }
  catch(Exception $e)
  {
    echo "Ajout ratée";
  }

}
elseif(array_key_exists('action', $_POST) && $_POST['action'] == 'recuperation'){
  $sql = 'Select * from CRENEAU where idC= :id';
  $stmt = $file_db->prepare($sql);
  $stmt->bindValue(':id', $_POST['idC']);
  $stmt->execute();
  $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
  print_r(json_encode($result));
}

elseif(array_key_exists('action', $_POST) && $_POST['action'] == 'modifier')
{
  $sql = 'UPDATE CRENEAU SET hdebut=:debut, hfin=:fin where idC= :id';
  $stmt = $file_db->prepare($sql);
  $stmt->bindValue(':id', $_POST['idC']);
  $stmt->bindParam(':debut', $_POST['hdebut']);
  $stmt->bindParam(':fin', $_POST['hfin']);
  try
  {
    $stmt->execute();
    echo "Modification réussi";
  }
  catch(Exception $e)
  {
    echo "Modification ratée";
  }

}
elseif(array_key_exists('action', $_POST) && $_POST['action'] == 'supprimer')
{
  $sql2= 'Select idJ,idP from JUGE where idC= :id';
  $stmt2 = $file_db->prepare($sql2);
  $stmt2->bindValue(':id', $_POST['idC']);
  $stmt2 ->execute();
  $res2 = $stmt2->fetchAll(\PDO::FETCH_ASSOC);
  if (empty($res2)==FALSE){
    foreach($res2 as $res2){
      $sql2='DELETE FROM JUGE where idP =:idP and idJ=:idJ and idC=:idC';
      $stmt2 = $file_db->prepare($sql2);
      $stmt2->bindValue(':idJ', $res2['idJ']);
      $stmt2->bindValue(':idP', $res2['idP']);
      $stmt2->bindValue(':idC', $_POST['idC']);
      $stmt2 ->execute();
    }
  }

  $sql = 'DELETE FROM CRENEAU where idC=:id';
  $stmt = $file_db->prepare($sql);
  $stmt->bindValue(':id', $_POST['idC']);
  try
  {
    $stmt->execute();
    echo "Suppression réussi";
  }
  catch(Exception $e)
  {
    echo "Suppression ratée";
  }
}
else
{
  $sth = $file_db->prepare("Select * from CRENEAU");
  $sth->execute();
  $result = $sth->fetchAll(\PDO::FETCH_ASSOC);
  print_r(json_encode($result));
}
$file_db = null;
?>
