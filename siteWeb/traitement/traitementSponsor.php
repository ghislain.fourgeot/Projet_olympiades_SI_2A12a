<?php
include "traitementAndroid/ConnectionBD.php";
if(array_key_exists('action', $_POST) && $_POST['action'] == 'ajouter'){

  $sql = 'Select max(idS) as idS from SPONSOR';
  $result = $file_db->prepare($sql);
  $result->execute();
  $res = $result->fetch(PDO::FETCH_ASSOC);
  $res['idS']+= 1;

  $sql = 'INSERT INTO SPONSOR VALUES(:id, :nom, :image, :type, :annee)';
  $stmt = $file_db->prepare($sql);
  $stmt->bindValue(':id', $res['idS']);
  $stmt->bindParam(':nom', $_POST['nomSponsor']);
  $stmt->bindParam(':image', $_POST['image']);
  $stmt->bindParam(':annee', $_POST['annee']);
  $stmt->bindParam(':type', $_POST['type']);
  try
  {
    $stmt->execute();
    echo "Ajout réussi";
  }
  catch(Exception $e)
  {
    echo "Ajout ratée";
  }

}
elseif(array_key_exists('action', $_POST) && $_POST['action'] == 'recuperation'){
  $sql = 'Select * from SPONSOR where idS= :id';
  $stmt = $file_db->prepare($sql);
  $stmt->bindValue(':id', $_POST['idS']);
  $stmt->execute();
  $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
  print_r(json_encode($result));
}
elseif(array_key_exists('action', $_POST) && $_POST['action'] == 'modifier'){

  $sql = 'UPDATE SPONSOR SET nom=:nom , image=:image , annee=:annee,type =:type   where idS=:id';
  $stmt = $file_db->prepare($sql);
  $stmt->bindValue(':id', $_POST['idS']);
  $stmt->bindParam(':nom', $_POST['nomSponsor']);
  $stmt->bindParam(':image', $_POST['image']);
  $stmt->bindParam(':type', $_POST['type']);
  $stmt->bindParam(':annee', $_POST['annee']);
  try
  {
    $stmt->execute();
    echo "Modification réussi";
  }
  catch(Exception $e)
  {
    echo "Modification ratée";
  }
}
elseif(array_key_exists('action', $_POST) && $_POST['action'] == 'supprimer'){

  $sql = 'DELETE FROM SPONSOR where idS=:id';
  $stmt = $file_db->prepare($sql);
  $stmt->bindValue(':id', $_POST['idS']);
  try
  {
    $stmt->execute();
    echo "Suppression réussi";
  }
  catch(Exception $e)
  {
    echo "Suppression ratée";
  }
}
else
{
  $sth = $file_db->prepare("Select * from SPONSOR");
  $sth->execute();
  $result = $sth->fetchAll(\PDO::FETCH_ASSOC);
  print_r(json_encode($result));
}

$file_db = null;
?>
