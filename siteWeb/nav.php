<nav class="white">
  <div class="nav-wrapper">
    <a href="index.php" class="brand-logo"><img src="images/logo.jpg"></a>
    <ul class="right hide-on-med-and-down">
      <li><a class="black-text" href="resultats.php">Résultat</a></li>
      <?php if(array_key_exists('admin', $_SESSION) && $_SESSION["admin"]){?>
      <li><a class="black-text" href="planning.php">Planning</a></li>
      <li><a class="black-text" href="gestion.php">Gestion</a></li>
      <li><a class="material-icons black-text" href="deconnexion.php">exit_to_app</a></li>
      <?php } else { ?>
      <li><a class="black-text" href="planningJury.php">Planning Jury</a></li>
      <li><a class="black-text" href="admin.php">Connexion Admin</a></li>
      <?php }?>
    </ul>
  </div>
</nav>
