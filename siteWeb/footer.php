<div id="footer" class="blue page-footer">
    <div class="blue footer-copyright">
      <div class="container">
        © 2018 Copyright 2A12-B
        <a class="grey-text text-lighten-4 right" href="http://www.univ-orleans.fr/iut-orleans/">IUT Orléans</a>
      </div>
    </div>
</div>
