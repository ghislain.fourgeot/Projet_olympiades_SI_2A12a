<?php session_start();
  if(array_key_exists('admin', $_SESSION) && $_SESSION["admin"])
  {
    header('Location: planning.php');
  } else {
    header('Location: planningJury.php');

  }
?>
<html>
  <head>
    <title>Accueil - OSI</title>
    <meta charset="utf-8">
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1.0">  -->
    <link rel="stylesheet" type="text/css" href="css/materialize.min.css">
    <link rel="stylesheet" type="text/css" href="css/materialize.css">
		<link href="css/style.css" rel="stylesheet">
  <link rel="icon" type="icon" href="../appAndroid/OlympiadeSI/app/src/main/res/drawable/launcher_icon.png">
  </head>

  <body>
    <?php include 'nav.php'; ?>
    <div id="wrap">
      <div id="main">

        <script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
        <script type="text/javascript" src="js/materialize.min.js"></script>
        <script>
              $(document).ready(function(){
              $('select').material_select();
                  });
        </script>

        <div id="zone-centrale">
            <div><a class="hoverable blue button" href="admin.php">Administrateur</a></div>
          </div>
      </div> <!-- stop main -->
    </div> <!-- stop wrap -->

  </div>
</div>
    <?php include 'footer.php'?>
  </body>
</html>
