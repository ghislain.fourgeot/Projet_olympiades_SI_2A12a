<?php session_start();
if(!(array_key_exists('admin', $_SESSION) && $_SESSION["admin"]))
{
  header('Location: admin.php');
}
 ?>
<!DOCTYPE html>
<html>
<head>
  <title>Gestion - OSI</title>
  <meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href="css/materialize.min.css">
  <link rel="stylesheet" type="text/css" href="css/materialize.css">
  <link rel="stylesheet" type="text/css" href="css/style.css">
  <link rel="icon" type="icon" href="../appAndroid/OlympiadeSI/app/src/main/res/drawable/launcher_icon.png">
</head>

<body>
  <?php include 'nav.php'; ?>
  <div id="wrap">
    <div id="main">
    <h3><b>Gestion</b></h3>

      <div id="onglets">
        <input id="actionProjet" name="actionProjet" type="hidden" value="ajouter">
        <input id="actionCreneau" name="actionCreneau" type="hidden" value="ajouter">
        <input id="actionEtudiant" name="actionEtudiant" type="hidden" value="ajouter">
        <input id="actionJury" name="actionJury" type="hidden" value="ajouter">
        <input id="actionJure" name="actionJure" type="hidden" value="ajouter">
        <input id="actionSponsor" name="actionSponsor" type="hidden" value="ajouter">

        <input id="idProjet" name="idProjet" type="hidden" value="">
        <input id="idEtudiant" name="idEtudiant" type="hidden" value="">
        <input id="idJure" name="" type="hidden" value="">
        <input id="idJury" name="" type="hidden" value="">
        <input id="idCreneau" name="" type="hidden" value="">
        <input id="idSponsor" name="idSponsor" type="hidden" value="">

        <ul id="tabs-swipe-demo" class="tabs">
          <li class="tab col s2"><a href="#projet">Projet</a></li>
          <li class="tab col s2"><a href="#etudiant">Etudiant</a></li>
          <li class="tab col s2"><a href="#jury">Jury</a></li>
          <li class="tab col s2"><a href="#jure">Juré</a></li>
          <li class="tab col s2"><a href="#creneau">Créneau</a></li>
          <li class="tab col s2"><a href="#sponsor">Sponsor</a></li>
          <li class="tab col s2"><a href="#importation">Importation</a></li>
        </ul>

        <div id="projet" class="col s12">
          <div class="row">
            <div class="row">
              <div class="col s7">
                <div class="input-field col s3">
                  <input id="nomProjet" type="text">
                  <label for="nomProjet" data-error="champ vide">Nom du projet</label>
                </div>
                <div class="input-field col s3">
                  <input id="image" type="text">
                  <label for="image" data-error="champ vide">Lien de l'image</label>
                </div>
                <div class="input-field col s3">
                  <input id="emplacement" type="text">
                  <label for="emplacement" data-error="champ vide">Emplacement</label>
                </div>
                <div class="row">
                  <div class="input-field col s6">
                    <textarea id="description" class="materialize-textarea"></textarea>
                    <label for="description" data-error="champ vide">Description</label>
                  </div>
                  <div class="col s3">
                    <button class="btn waves-effect waves-light" id="sendProjet" name="action">Envoyer
                      <i class="material-icons right">done</i>
                    </button>
                  </div>
                </div>
              </div>
              <div id="resultatsProjet" class="col s5">
              </div>
            </div>
          </div>
        </div>

        <div id="etudiant" class="col s6">
          <div class="row">
            <div class="row">
              <div class="col s7">
                <div class="input-field col s3">
                  <input id="nom" type="text">
                  <label for="nom" data-error="champ vide">Nom</label>
                </div>
                <div class="input-field col s3">
                  <input id="prenom" type="text">
                  <label for="prenom" data-error="champ vide">Prénom</label>
                </div>
                <div class="input-field col s3">
                  <input id="lycee" type="text">
                  <label for="lycee" data-error="champ vide">Lycée</label>
                </div>
                <div class="input-field col s3">
                  <input id="formation" type="text">
                  <label for="formation" data-error="champ vide">Formation</label>
                </div>
                <div class="row">
                  <div class="input-field col s3">
                    <select id="listeProjets">
                    </select>
                    <label>Projets</label>
                  </div>
                  <div class="col s3">
                    <button class="btn waves-effect waves-light" id="sendEtudiant" name="action">Envoyer
                      <i class="material-icons right">done</i>
                    </button>
                  </div>
                </div>
              </div>
              <div id="resultatsEtudiant" class="col s5">
              </div>
            </div>
          </div>
        </div>

        <div id="jury" class="col s6">
          <div class="row">
            <div class="row">
              <div class="col s7">
                <div class="input-field col s3">
                  <input id="identifiantJury" type="text">
                  <label for="identifiantJury" data-error="champ vide">Identifiant</label>
                </div>
                <div class="input-field col s3">
                  <input id="motPasseJury" type="password">
                  <label for="motPasseJury" data-error="champ vide">Mot de passe</label>
                </div>
                <div class="input-field col s3">
                  <input id="confirmMotPasseJury" type="password">
                  <label for="confirmMotPasseJury" data-error="champ vide">Confirmation Mot de passe</label>
                </div>
                <div class="row">
                  <div class="col s3">
                    <button class="btn waves-effect waves-light" id="sendJury" name="action">Envoyer
                      <i class="material-icons right">done</i>
                    </button>
                  </div>
                </div>
              </div>
              <div id="resultatsJury" class="col s5">
              </div>
            </div>
          </div>
        </div>

        <div id="jure" class="col s6">
          <div class="row">
            <div class="row">
              <div class="col s7">
                <div class="input-field col s3">
                  <input id="nomJure" type="text">
                  <label for="nomJure" data-error="champ vide">Nom</label>
                </div>
                <div class="input-field col s3">
                  <input id="prenomJure" type="text">
                  <label for="prenomJure" data-error="champ vide">Prénom</label>
                </div>
                <div class="input-field col s3">
                  <input id="origineJure" type="text">
                  <label for="origineJure" data-error="champ vide">Origine</label>
                </div>
                <div class="row">
                  <div class="input-field col s3">
                    <select id="listeJury">
                    </select>
                    <label>Jury</label>
                  </div>
                  <div class="col s3">
                    <button class="btn waves-effect waves-light" id="sendJure" name="action">Envoyer
                      <i class="material-icons right">done</i>
                    </button>
                  </div>
                </div>
              </div>
              <div id="resultatsJure" class="col s5">
              </div>
            </div>
          </div>
        </div>

        <div id="creneau" class="col s6">
          <div class="row">
            <div class="row">
              <div class="col s7">
                <div class="input-field col s4">
                  <input id="hdebut" type="text" class="timepicker">
                  <label for="hdebut" data-error="champ vide">Heure Début</label>
                </div>
                <div class="input-field col s4">
                  <input id="hfin" type="text" class="timepicker">
                  <label for="hfin" data-error="champ vide">Heure Fin</label>
                </div>
                <div class="row">
                  <div class="col s3">
                    <button class="btn waves-effect waves-light" id="sendCreneau" name="action">Envoyer
                      <i class="material-icons right">done</i>
                    </button>
                  </div>
                </div>
              </div>
              <div id="resultatsCreneau" class="col s5">
              </div>
            </div>
          </div>
        </div>

        <div id="sponsor" class="col s6">
          <div class="row">
            <div class="row">
              <div class="col s7">
                <div class="input-field col s3">
                  <input id="nomSponsor" type="text">
                  <label for="nomSponsor" data-error="champ vide">Nom</label>
                </div>
                <div class="input-field col s3">
                  <select id="listeType">
                    <option value="1">Primaire</option>
                    <option value="2">Secondaire</option>
                  </select>
                  <label>Type</label>
                </div>
                <div class="input-field col s3">
                  <input id="imageSponsor" type="text">
                  <label for="imageSponsor" data-error="champ vide">Image</label>
                </div>
                <div class="input-field col s3">
                  <input id="anneeSponsor" type="text">
                  <label for="anneeSponsor" data-error="champ vide">Année</label>
                </div>
                <input type="hidden" id="idSponsor" name="idS">
                <div class="row">
                  <div class="col s3">
                    <button class="btn waves-effect waves-light" id="sendSponsor" name="action">Envoyer
                      <i class="material-icons right">done</i>
                    </button>
                  </div>
                </div>
              </div>
              <div id="resultatsSponsor" class="col s5">
              </div>
            </div>
          </div>
        </div>

        <div id="importation" class="col s6">
         <div class="row">
         <form enctype="multipart/form-data" action="traitement/traitementImportation.php" method="post">
            <div class="file-field input-field col s3">
             <div class="btn">
              <span>Fichier CSV</span>
              <input id="fichierCSV" name="file" type="file">
            </div>
            <div class="file-path-wrapper">
              <input class="file-path validate" type="text">
            </div>
          </div>
          <div class="col s3">
            <div class="input-field col s12">
              <select name="table">
                <option value="" disabled selected>Table dans laquelle importer le CSV</option>
                <option value="ETUDIANT">ETUDIANT</option>
                <option value="PROJET">PROJET</option>
                <option value="MEMBRE_DU_JURY">MEMBRE_DU_JURY</option>
                <option value="CRENEAU">CRENEAU</option>
              </select>
              <label>Table</label>
            </div>
          </div>
        </div>
        <div>
          <button class="btn waves-effect waves-light" id="sendCSV" name="action">Envoyer
            <i class="material-icons right">done</i>
          </button>
        </div>
      </form>
    </div>
	</div>

      <div id="error"></div>

    </div> <!-- end main-->
  </div>  <!-- end wrap-->
  <script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
  <script type="text/javascript" src="js/materialize.min.js"></script>
  <script type="text/javascript" src="js/materialize.js"></script>
  <script>
    $(document).ready(function(){
      //Initatialisation du timePicker
      $('.timepicker').pickatime({
        default: 'now',
        fromnow: 0,
        twelvehour: false,
        donetext: 'OK',
        cleartext: 'Clear',
        canceltext: 'Cancel',
        autoclose: false,
        ampmclickable: true,
        aftershow: function(){}
      });

      $('select').material_select();
      update();

      //Cette function permet d'envoyer un creneau en base de données
      $("#sendCreneau").click(function(){
        var error = false;
        if(document.getElementById('hdebut').value === '')
        {
          $('#hdebut').addClass('invalid');
          error = true;
        }
        else
        {
          $('#hdebut').removeClass('invalid');
        }

        if(document.getElementById('hfin').value === '')
        {
          $('#hfin').addClass('invalid');
          error = true;
        }
        else
        {
          $('#hfin').removeClass('invalid');
        }


        if(!error)
        {
          console.log(document.getElementById('actionCreneau').value);
          if(document.getElementById('actionCreneau').value === 'ajouter')
          {
            $.ajax({
               url : '/projet/siteWeb/traitement/traitementCreneau.php',
                 type : 'POST', // Le type de la requête HTTP, ici devenu POST
                 data : 'hdebut=' +  document.getElementById('hdebut').value
                 + '&hfin=' + document.getElementById('hfin').value
                 + '&action=' + document.getElementById('actionCreneau').value,
                 dataType : 'html',
                 success : function(code_html, statut)
                 {
                  Materialize.toast(code_html, 2000);
                  document.getElementById('hdebut').value = '';
                  document.getElementById('hfin').value = '';
                  update();
                },
            });
          }
          else if(document.getElementById('actionCreneau').value === 'modifier')
          {
            console.log(document.getElementById('idCreneau').value);
            $.ajax({
               url : '/projet/siteWeb/traitement/traitementCreneau.php',
                 type : 'POST', // Le type de la requête HTTP, ici devenu POST
                 data : 'idC=' +  document.getElementById('idCreneau').value
                 + '&hdebut=' + document.getElementById('hdebut').value
                 + '&hfin=' + document.getElementById('hfin').value
                 + '&action=' + document.getElementById('actionCreneau').value,
                 dataType : 'html',
                 success : function(code_html, statut)
                 {
                  Materialize.toast(code_html, 2000);
                  document.getElementById('hdebut').value = '';
                  document.getElementById('hfin').value = '';
                  update();
                },
              });
          }
        }
        document.getElementById('actionCreneau').value = 'ajouter';
      });

      //Cette function permet d'envoyer un jury en base de données
      $("#sendJury").click(function(){
        var error = false;
        if(document.getElementById('identifiantJury').value === '')
        {
          $('#identifiantJury').addClass('invalid');
          error = true;
        }
        else
        {
          $('#identifiantJury').removeClass('invalid');
        }

        if(document.getElementById('motPasseJury').value === '')
        {
          $('#motPasseJury').addClass('invalid');
          error = true;
        }
        else
        {
          $('#motPasseJury').removeClass('invalid');
        }

        if(document.getElementById('confirmMotPasseJury').value === '')
        {
          $('#confirmMotPasseJury').addClass('invalid');
          error = true;
        }
        else
        {
          $('#confirmMotPasseJury').removeClass('invalid');
        }


        if(!error)
        {
          if(document.getElementById('confirmMotPasseJury').value === document.getElementById('motPasseJury').value)
          {
            if(document.getElementById('actionJury').value === 'ajouter')
            {
              $.ajax({
               url : '/projet/siteWeb/traitement/traitementJury.php',
                 type : 'POST', // Le type de la requête HTTP, ici devenu POST
                 data : 'identifiant=' +  document.getElementById('identifiantJury').value
                 + '&mdp=' + document.getElementById('confirmMotPasseJury').value
                 + '&action=' + document.getElementById('actionJury').value,
                 dataType : 'html',
                 success : function(code_html, statut)
                 {
                  Materialize.toast(code_html, 2000);
                  document.getElementById('identifiantJury').value = '';
                  document.getElementById('confirmMotPasseJury').value = '';
                  document.getElementById('motPasseJury').value = '';
                  update();
                },
              });
            }
            else if(document.getElementById('actionJury').value === 'modifier')
            {
              $.ajax({
               url : '/projet/siteWeb/traitement/traitementJury.php',
                 type : 'POST', // Le type de la requête HTTP, ici devenu POST
                 data : 'identifiant=' +  document.getElementById('identifiantJury').value
                 + '&mdp=' + document.getElementById('confirmMotPasseJury').value
                 + '&idJ=' + document.getElementById('idJury').value
                 + '&action=' + document.getElementById('actionJury').value,
                 dataType : 'html',
                 success : function(code_html, statut)
                 {
                  Materialize.toast(code_html, 2000);
                  document.getElementById('identifiantJury').value = '';
                  document.getElementById('confirmMotPasseJury').value = '';
                  document.getElementById('motPasseJury').value = '';
                  update();
                },
              });
            }
          }
          else
          {
            Materialize.toast("Les mots de passe de correspondent pas", 2000);
          }
        }
        document.getElementById('actionJury').value = 'ajouter';
      });

      // Cette fonction permet d'envoyer un juré en base de données
      $("#sendJure").click(function(){
        var error = false;
        if(document.getElementById('nomJure').value === '')
        {
          $('#nomJure').addClass('invalid');
          error = true;
        }
        else
        {
          $('#nomJure').removeClass('invalid');
        }

        if(document.getElementById('origineJure').value === '')
        {
          $('#origineJure').addClass('invalid');
          error = true;
        }
        else
        {
          $('#origineJure').removeClass('invalid');
        }

        if(document.getElementById('prenomJure').value === '')
        {
          $('#prenomJure').addClass('invalid');
          error = true;
        }
        else
        {
          $('#prenomJure').removeClass('invalid');
        }

        if(!error)
        {
          if(document.getElementById('actionJure').value === 'ajouter')
          {
            var select = document.getElementById("listeJury");
            $.ajax({
             url : '/projet/siteWeb/traitement/traitementJure.php',
           type : 'POST', // Le type de la requête HTTP, ici devenu POST
           data : 'nomJure=' +  document.getElementById('nomJure').value
           + '&prenomJure=' + document.getElementById('prenomJure').value
           + '&origine=' + document.getElementById('origineJure').value
           + '&idJ=' + select.options[select.selectedIndex].text
           + '&action=' + document.getElementById('actionJure').value,
           dataType : 'html',
           success : function(code_html, statut)
           {
            Materialize.toast(code_html, 2000);
            document.getElementById('nomJure').value = '';
            document.getElementById('prenomJure').value = '';
            document.getElementById('origineJure').value = '';
            update();
          },
        });
          }
          else if(document.getElementById('actionJure').value === 'modifier')
          {
            var select = document.getElementById("listeJury");
            $.ajax({
             url : '/projet/siteWeb/traitement/traitementJure.php',
           type : 'POST', // Le type de la requête HTTP, ici devenu POST
           data : 'nomJure=' +  document.getElementById('nomJure').value
           + '&prenomJure=' + document.getElementById('prenomJure').value
           + '&origine=' + document.getElementById('origineJure').value
           + '&idJ=' + select.options[select.selectedIndex].text
           + '&idM=' + document.getElementById('idJure').value
           + '&action=' + document.getElementById('actionJure').value,
           dataType : 'html',
           success : function(code_html, statut)
           {
            Materialize.toast(code_html, 2000);
            document.getElementById('nomJure').value = '';
            document.getElementById('prenomJure').value = '';
            document.getElementById('origineJure').value = '';
            update();
          },
        });
          }
        }
        document.getElementById('actionJure').value = 'ajouter'
      });

      // Cette fonction permet d'envoyer un sponsor en base de données
      $("#sendSponsor").click(function(){
        var error = false;
        if(document.getElementById('nomSponsor').value === '')
        {
          $('#nomSponsor').addClass('invalid');
          error = true;
        }
        else
        {
          $('#nomSponsor').removeClass('invalid');
        }

        if(document.getElementById('imageSponsor').value === '')
        {
          $('#imageSponsor').addClass('invalid');
          error = true;
        }
        else
        {
          $('#imageSponsor').removeClass('invalid');
        }

        if(document.getElementById('anneeSponsor').value === '')
        {
          $('#anneeSponsor').addClass('invalid');
          error = true;
        }
        else
        {
          $('#anneeSponsor').removeClass('invalid');
        }

        if(!error)
        {
          if(document.getElementById('actionSponsor').value === 'ajouter')
          {
            var select = document.getElementById("listeType");

            $.ajax({
             url : '/projet/siteWeb/traitement/traitementSponsor.php',
           type : 'POST', // Le type de la requête HTTP, ici devenu POST
           data : 'nomSponsor=' +  document.getElementById('nomSponsor').value
           + '&image=' + document.getElementById('imageSponsor').value
           + '&annee=' + document.getElementById('anneeSponsor').value
           + '&type=' + select.options[select.selectedIndex].text
           + '&action=' + document.getElementById('actionSponsor').value,
           dataType : 'html',
           success : function(code_html, statut)
           {
            Materialize.toast(code_html, 2000);
            document.getElementById('imageSponsor').value = '';
            document.getElementById('nomSponsor').value = '';
            document.getElementById('anneeSponsor').value = '';
            update();
          },
        });
          }
          else if (document.getElementById('actionSponsor').value === 'modifier')
          {
            var select = document.getElementById("listeType");

            $.ajax({
             url : '/projet/siteWeb/traitement/traitementSponsor.php',
           type : 'POST', // Le type de la requête HTTP, ici devenu POST
           data : 'nomSponsor=' +  document.getElementById('nomSponsor').value
           + '&image=' + document.getElementById('imageSponsor').value
           + '&idS=' + document.getElementById('idSponsor').value
           + '&annee=' + document.getElementById('anneeSponsor').value
           + '&type=' + select.options[select.selectedIndex].text
           + '&action=' + document.getElementById('actionSponsor').value,
           dataType : 'html',
           success : function(code_html, statut)
           {
            Materialize.toast(code_html, 2000);
            document.getElementById('imageSponsor').value = '';
            document.getElementById('nomSponsor').value = '';
            document.getElementById('anneeSponsor').value = '';
            document.getElementById('formation').value = '';
            update();
          },
        });
          }
        }
        document.getElementById('actionSponsor').value = 'ajouter';
      });

      // Cette fonction permet d'envoyer un étudiant en base de données
      $("#sendEtudiant").click(function(){
        var error = false;
        if(document.getElementById('nom').value === '')
        {
          $('#nom').addClass('invalid');
          error = true;
        }
        else
        {
          $('#nom').removeClass('invalid');
        }

        if(document.getElementById('prenom').value === '')
        {
          $('#prenom').addClass('invalid');
          error = true;
        }
        else
        {
          $('#prenom').removeClass('invalid');
        }

        if(document.getElementById('lycee').value === '')
        {
          $('#lycee').addClass('invalid');
          error = true;
        }
        else
        {
          $('#lycee').removeClass('invalid');
        }

        if(document.getElementById('formation').value === '')
        {
          $('#formation').addClass('invalid');
          error = true;
        }
        else
        {
          $('#formation').removeClass('invalid');
        }

        if(!error)
        {
          if(document.getElementById('actionEtudiant').value === 'ajouter')
          {
            var select = document.getElementById("listeProjets");

            $.ajax({
             url : '/projet/siteWeb/traitement/traitementEtudiant.php',
           type : 'POST', // Le type de la requête HTTP, ici devenu POST
           data : 'idP=' + select.options[select.selectedIndex].value
           + '&nomEtudiant=' + document.getElementById("nom").value
           + '&prenomEtudiant=' + document.getElementById("prenom").value
           + '&lycee=' + document.getElementById("lycee").value
           + '&formation=' + document.getElementById("formation").value
           + '&action=' + document.getElementById('actionEtudiant').value,
           dataType : 'html',
           success : function(code_html, statut)
           {
            Materialize.toast(code_html, 2000);
            document.getElementById('nom').value = '';
            document.getElementById('prenom').value = '';
            document.getElementById('lycee').value = '';
            document.getElementById('formation').value = '';
            update();
          },
        });
          }
          else if(document.getElementById('actionEtudiant').value === 'modifier')
          {
            var select = document.getElementById("listeProjets");
            $.ajax({
             url : '/projet/siteWeb/traitement/traitementEtudiant.php',
           type : 'POST', // Le type de la requête HTTP, ici devenu POST
           data : 'idP=' + select.options[select.selectedIndex].value
           + '&idE=' + document.getElementById('idEtudiant').value
           + '&nomEtudiant=' + document.getElementById("nom").value
           + '&prenomEtudiant=' + document.getElementById("prenom").value
           + '&lycee=' + document.getElementById("lycee").value
           + '&formation=' + document.getElementById("formation").value
           + '&action=' + document.getElementById('actionEtudiant').value,
           dataType : 'html',
           success : function(code_html, statut)
           {
            Materialize.toast(code_html, 2000);
            document.getElementById('nom').value = '';
            document.getElementById('prenom').value = '';
            document.getElementById('lycee').value = '';
            document.getElementById('formation').value = '';
            update();
          },
        });
          }
          document.getElementById('actionEtudiant').value='ajouter';
        }


      });

      // Cette fonction permet d'envoyer un projet en base de données
      $("#sendProjet").click(function(){
        var error = false;

        if(document.getElementById('nomProjet').value === '')
        {
          $('#nomProjet').addClass('invalid');
          error = true;
        }
        else
        {
          $('#nomProjet').removeClass('invalid');
        }

        if(document.getElementById('image').value === '')
        {
          $('#image').addClass('invalid');
          error = true;
        }
        else
        {
          $('#image').removeClass('invalid');
        }

        if(document.getElementById('description').value === '')
        {
          $('#description').addClass('invalid');
          error = true;
        }
        else
        {
          $('#description').removeClass('invalid');
        }

        if(document.getElementById('emplacement').value === '')
        {
          $('#emplacement').addClass('invalid');
          error = true;
        }
        else
        {
          $('#emplacement').removeClass('invalid');
        }

        if(!error)
        {
          if(document.getElementById('actionProjet').value === 'ajouter')
          {
            $.ajax({
             url : '/projet/siteWeb/traitement/traitementProjet.php',
           type : 'POST', // Le type de la requête HTTP, ici devenu POST
           data : 'nomProjet=' + document.getElementById('nomProjet').value
           + '&image=' + document.getElementById('image').value
           + '&description=' + document.getElementById('description').value
           + '&emplacement=' + document.getElementById('emplacement').value
           + '&action=' + document.getElementById('actionProjet').value,
           dataType : 'html',
           success : function(code_html, statut)
           {
            Materialize.toast(code_html, 2000);
            document.getElementById('nomProjet').value = '';
            document.getElementById('image').value = '';
            document.getElementById('description').value = '';
            document.getElementById('emplacement').value = '';
            update();
          },
        });
          }
          else if (document.getElementById('actionProjet').value === 'modifier')
          {
            $.ajax({
             url : '/projet/siteWeb/traitement/traitementProjet.php',
           type : 'POST', // Le type de la requête HTTP, ici devenu POST
           data : 'nomProjet=' + document.getElementById('nomProjet').value
           + '&image=' + document.getElementById('image').value
           + '&description=' + document.getElementById('description').value
           + '&emplacement=' + document.getElementById('emplacement').value
           + '&idP=' + document.getElementById('idProjet').value
           + '&action=' + document.getElementById('actionProjet').value,

           dataType : 'html',
           success : function(code_html, statut)
           {
            Materialize.toast(code_html, 2000);
            document.getElementById('nomProjet').value = '';
            document.getElementById('image').value = '';
            document.getElementById('description').value = '';
            document.getElementById('emplacement').value = '';
            update();
          },
        });
          }
        }
        document.getElementById('actionProjet').value='ajouter';
      });


    });

// Cette fonction permet de mettre a jour tout les onglets de la page de gestion un fois qu'elle est appelée
function update()
{
  $.ajax({
    url : '/projet/siteWeb/traitement/traitementProjet.php',
    type : 'POST',
    success : function(code_html, statut)
    {
      var parsing = JSON.parse(code_html);
      var element = $('#resultatsProjet');
      var html = "<ul class=\"collection\">";
      for(var projet in parsing) {
        html += "<li class=\"collection-item avatar\">";
        html += "<div class=\"row\">";
        html += "<div class=\"col s6\" >";
        html += "<img src=\" ../appAndroid/OlympiadeSI/app/src/main/assets/images/" + parsing[projet].image + "\" alt=\"\" class=\"circle\">";
        html += "<span class=\"title\">" + parsing[projet].nomP + "<span>";
        html += "<p> Emplacement: " + parsing[projet].emplacement + "</p>";
        html += "</div>"
        html += "<div class=\"container col s6\" >";
        html += "<button class=\"btn waves-effect waves-light left-align blue\" id=\"ModifierProjet\" name=\"action\" onclick=\"ModifierProjet("+ parsing[projet].idP +")\">";
        html += "<i class=\"material-icons right\">settings</i>";
        html += "</button>";
        html += "<button class=\"btn waves-effect waves-light left-align red\" id=\"SupprimerProjet\" name=\"action\" onclick=\"SupprimerProjet("+ parsing[projet].idP +")\">";
        html += "<i class=\"material-icons right\">clear</i>";
        html += "</button>";
        html += "</div>";
        html += "</div>";
        html += "</li>";
      }
      html += "</ul>";
      element.html(html);
    },
  });

  $.ajax({
    url : '/projet/siteWeb/traitement/traitementEtudiant.php',
    type : 'POST',
    success : function(code_html, statut)
    {
      var parsing = JSON.parse(code_html);
      var element = $('#resultatsEtudiant');
      var html = "<ul class=\"collection\">";
      for(var etudiant in parsing) {
        html += "<li class=\"collection-item avatar\">";
        html += "<div class=\"row\">";
        html += "<div class=\"col s6\" >";
        html += "<span class=\"title\">" + parsing[etudiant].prenom + " " + parsing[etudiant].nom + "<span>";
        html += "<p> Projet: " + parsing[etudiant].nomP + "</p>";
        html += "<p> Lycée: " + parsing[etudiant].lycee + "</p>";
        html += "<p> Filière: " + parsing[etudiant].formation + "</p>";
        html += "</div>"
        html += "<div class=\"container col s6\" >";
        html += "<button class=\"btn waves-effect waves-light left-align blue\" id=\"ModifierProjet\" name=\"action\" onclick=\"ModifierEtudiant("+ parsing[etudiant].idE +")\">";
        html += "<i class=\"material-icons right\">settings</i>";
        html += "</button>";
        html += "<button class=\"btn waves-effect waves-light left-align red\" id=\"SupprimerProjet\" name=\"action\" onclick=\"SupprimerEtudiant("+ parsing[etudiant].idE +")\">";
        html += "<i class=\"material-icons right\">clear</i>";
        html += "</button>";
        html += "</div>";
        html += "</div>";
        html += "</li>";
      }
      html += "</ul>";
      element.html(html);
    },
  });

  document.getElementById('actionEtudiant').value = 'recupProjets';
  $.ajax({
    url : '/projet/siteWeb/traitement/traitementEtudiant.php',
    type : 'POST',
    data : 'action=' + document.getElementById('actionEtudiant').value,
    dataType : 'html',
    success : function(code_html, statut)
    {
      var parsing = JSON.parse(code_html);
      var element = $('#listeProjets');
      element.html('');
      for(var projet in parsing)
      {
        element.append($('<option>', {
          value: parsing[projet].idP,
          text: parsing[projet].nomP
        }));
      }
      element.material_select();
    },
  });
  document.getElementById('actionEtudiant').value = 'ajouter';

  $.ajax({
    url : '/projet/siteWeb/traitement/traitementSponsor.php',
    type : 'POST',
    success : function(code_html, statut)
    {
      var parsing = JSON.parse(code_html);
      var element = $('#resultatsSponsor');
      var html = "<ul class=\"collection\">";
      for(var sponsor in parsing) {
        html += "<li class=\"collection-item avatar\">";
        html += "<div class=\"row\">";
        html += "<div class=\"col s6\" >";
        html += "<img src=\"" + parsing[sponsor].image + "\" alt=\"\" class=\"circle\">";
        html += "<span class=\"title\">" + parsing[sponsor].nom + "<span>";
        html += "<p> Année: " + parsing[sponsor].annee + "</p>";
        html += "</div>"
        html += "<div class=\"container col s6\" >";
        html += "<button class=\"btn waves-effect waves-light left-align blue\" id=\"ModifierProjet\" name=\"action\" onclick=\"ModifierSponsor("+ parsing[sponsor].idS +")\">";
        html += "<i class=\"material-icons right\">settings</i>";
        html += "</button>";
        html += "<button class=\"btn waves-effect waves-light left-align red\" id=\"SupprimerProjet\" name=\"action\" onclick=\"SupprimerSponsor("+ parsing[sponsor].idS +")\">";
        html += "<i class=\"material-icons right\">clear</i>";
        html += "</button>";
        html += "</div>";
        html += "</div>";
        html += "</li>";
      }
      html += "</ul>";
      element.html(html);
    },
  });

  document.getElementById('actionJure').value = 'recupJury';
  $.ajax({
    url : '/projet/siteWeb/traitement/traitementJure.php',
    type : 'POST',
    data : 'action=' + document.getElementById('actionJure').value,
    dataType : 'html',
    success : function(code_html, statut)
    {
      var parsing = JSON.parse(code_html);
      var element = $('#listeJury');
      element.html('');
      for(var jury in parsing)
      {
        element.append($('<option>', {
          value: parsing[jury].idJ,
          text: parsing[jury].identifiant
        }));
      }
      element.material_select();
    },
  });
  document.getElementById('actionJure').value = 'ajouter';

  $.ajax({
    url : '/projet/siteWeb/traitement/traitementJure.php',
    type : 'POST',
    success : function(code_html, statut)
    {
      var parsing = JSON.parse(code_html);
      var element = $('#resultatsJure');
      var html = "<ul class=\"collection\">";
      for(var jure in parsing) {
        html += "<li class=\"collection-item avatar\">";
        html += "<div class=\"row\">";
        html += "<div class=\"col s6\" >";
        html += "<span class=\"title\">" + parsing[jure].prenom + " " + parsing[jure].nom + "<span>";
        html += "<p> Origine: " + parsing[jure].origine + "</p>";
        html += "<p> Jury n°" + parsing[jure].idJ + "</p>";
        html += "</div>"
        html += "<div class=\"container col s6\" >";
        html += "<button class=\"btn waves-effect waves-light left-align blue\" id=\"ModifierProjet\" name=\"action\" onclick=\"ModifierJure("+ parsing[jure].idM +")\">";
        html += "<i class=\"material-icons right\">settings</i>";
        html += "</button>";
        html += "<button class=\"btn waves-effect waves-light left-align red\" id=\"SupprimerProjet\" name=\"action\" onclick=\"SupprimerJure("+ parsing[jure].idM +")\">";
        html += "<i class=\"material-icons right\">clear</i>";
        html += "</button>";
        html += "</div>";
        html += "</div>";
        html += "</li>";
      }
      html += "</ul>";
      element.html(html);
    },
  });

  $.ajax({
    url : '/projet/siteWeb/traitement/traitementJury.php',
    type : 'POST',
    success : function(code_html, statut)
    {
      var parsing = JSON.parse(code_html);
      var element = $('#resultatsJury');
      var html = "<ul class=\"collection\">";
      for(var jury in parsing) {
        html += "<li class=\"collection-item avatar\">";
        html += "<div class=\"row\">";
        html += "<div class=\"col s6\" >";
        html += "<span class=\"title\">Jury n°" + parsing[jury].idJ + "<span>";
        html += "<p> Identifiant: " + parsing[jury].identifiant + "</p>";
        html += "</div>"
        html += "<div class=\"container col s6\" >";
        html += "<button class=\"btn waves-effect waves-light left-align blue\" id=\"ModifierProjet\" name=\"action\" onclick=\"ModifierJury("+ parsing[jury].idJ +")\">";
        html += "<i class=\"material-icons right\">settings</i>";
        html += "</button>";
        html += "<button class=\"btn waves-effect waves-light left-align red\" id=\"SupprimerProjet\" name=\"action\" onclick=\"SupprimerJury("+ parsing[jury].idJ +")\">";
        html += "<i class=\"material-icons right\">clear</i>";
        html += "</button>";
        html += "</div>";
        html += "</div>";
        html += "</li>";
      }
      html += "</ul>";
      element.html(html);
    },
  });

  $.ajax({
    url : '/projet/siteWeb/traitement/traitementCreneau.php',
    type : 'POST',
    success : function(code_html, statut)
    {
      var parsing = JSON.parse(code_html);
      var element = $('#resultatsCreneau');
      var html = "<ul class=\"collection\">";
      for(var creneau in parsing) {
        html += "<li class=\"collection-item avatar\">";
        html += "<div class=\"row\">";
        html += "<div class=\"col s6\" >";
        html += "<span class=\"title\">Creneau n°" + parsing[creneau].idC + "<span>";
        html += "<p> Heure Debut: " + parsing[creneau].hdebut + "</p>";
        html += "<p> Heure Fin: " + parsing[creneau].hfin + "</p>";
        html += "</div>"
        html += "<div class=\"container col s6\" >";
        html += "<button class=\"btn waves-effect waves-light left-align blue\" id=\"ModifierProjet\" name=\"action\" onclick=\"ModifierCreneau("+ parsing[creneau].idC +")\">";
        html += "<i class=\"material-icons right\">settings</i>";
        html += "</button>";
        html += "<button class=\"btn waves-effect waves-light left-align red\" id=\"SupprimerProjet\" name=\"action\" onclick=\"SupprimerCreneau("+ parsing[creneau].idC +")\">";
        html += "<i class=\"material-icons right\">clear</i>";
        html += "</button>";
        html += "</div>";
        html += "</div>";
        html += "</li>";
      }
      html += "</ul>";
      element.html(html);
    },
  });

}

// Cette fonction prend en parametre un id de Projet
// Et permet de charger un projet sur la page en vue de le modifier
function ModifierProjet(idP)
{
  document.getElementById('actionProjet').value='recuperation';
  document.getElementById('idProjet').value=idP;
  $.ajax({
   url : '/projet/siteWeb/traitement/traitementProjet.php',
         type : 'POST', // Le type de la requête HTTP, ici devenu POST
         data : 'idP=' + idP +'&action=' + document.getElementById('actionProjet').value,
         dataType : 'html',
         success : function(code_html, statut)
         {
          var parsing = JSON.parse(code_html);
          document.getElementById('nomProjet').value = parsing[0].nomP;
          document.getElementById('image').value = parsing[0].image;
          document.getElementById('description').value = parsing[0].description;
          document.getElementById('emplacement').value = parsing[0].emplacement;
          update();
        },
      });
  document.getElementById('actionProjet').value='modifier';
}


// Cette fonction prend en parametre un id de Projet
// Et permet de supprimer un projet
function SupprimerProjet(idP)
{
  document.getElementById('actionProjet').value='supprimer';
  $.ajax({
   url : '/projet/siteWeb/traitement/traitementProjet.php',
         type : 'POST', // Le type de la requête HTTP, ici devenu POST
         data : 'idP=' + idP +'&action=' + document.getElementById('actionProjet').value,
         dataType : 'html',
         success : function(code_html, statut)
         {
          Materialize.toast(code_html, 2000);
          update();
        }
      });
  document.getElementById('actionProjet').value='ajouter';
}

// Cette fonction prend en parametre un id d'étudiant
// Et permet de charger un étudiant sur la page en vue de le modifier
function ModifierEtudiant(idE)
{
  document.getElementById('actionEtudiant').value='recuperation';
  document.getElementById('idEtudiant').value=idE;
  var element = document.getElementById('listeProjets');
  $.ajax({
   url : '/projet/siteWeb/traitement/traitementEtudiant.php',
         type : 'POST', // Le type de la requête HTTP, ici devenu POST
         data : 'idE=' + idE +'&action=' + document.getElementById('actionEtudiant').value,
         dataType : 'html',
         success : function(code_html, statut)
         {
          var parsing = JSON.parse(code_html);
          element.selectedIndex = parsing[0].idP;
          $('select').material_select();
          document.getElementById('nom').value = parsing[0].nom;
          document.getElementById('prenom').value = parsing[0].prenom;
          document.getElementById('lycee').value = parsing[0].lycee;
          document.getElementById('formation').value = parsing[0].formation;
        },
      });
  document.getElementById('actionEtudiant').value='modifier';
}

// Cette fonction prend en parametre un id d'étudiant
// Et permet de supprimer un étudiant
function SupprimerEtudiant(idE)
{
  document.getElementById('actionEtudiant').value='supprimer';
  $.ajax({
   url : '/projet/siteWeb/traitement/traitementEtudiant.php',
         type : 'POST', // Le type de la requête HTTP, ici devenu POST
         data : 'idE=' + idE +'&action=' + document.getElementById('actionEtudiant').value,
         dataType : 'html',
         success : function(code_html, statut)
         {
          Materialize.toast(code_html, 2000);
          update();
        }
      });
  document.getElementById('actionEtudiant').value='ajouter';
}

// Cette fonction prend en parametre un id de sponsor
// Et permet de charger un sponser sur la page en vue de le modifier
function ModifierSponsor(idSponsor)
{
  document.getElementById('actionSponsor').value='recuperation';
  document.getElementById('idSponsor').value=idSponsor;
  $.ajax({
   url : '/projet/siteWeb/traitement/traitementSponsor.php',
         type : 'POST', // Le type de la requête HTTP, ici devenu POST
         data : 'idS=' + idSponsor +'&action=' + document.getElementById('actionSponsor').value,
         dataType : 'html',
         success : function(code_html, statut)
         {
          var parsing = JSON.parse(code_html);
          if(parsing[0].type === "Primaire" || parsing[0].type === null)
          {
            $('#listeType').val("1");
          }
          else
          {
            $('#listeType').val("2");
          }
          $('select').material_select();
          document.getElementById('nomSponsor').value = parsing[0].nom ;
          document.getElementById('imageSponsor').value = parsing[0].image;
          document.getElementById('anneeSponsor').value = parsing[0].annee;
          document.getElementById('formation').value = parsing[0].formation;
        },
      });
  document.getElementById('actionSponsor').value='modifier';
}

// Cette fonction prend en parametre un id de sponsor
// et permet de supprimer un Sponsor
function SupprimerSponsor(idSponsor)
{
  document.getElementById('actionSponsor').value='supprimer';
  $.ajax({
   url : '/projet/siteWeb/traitement/traitementSponsor.php',
         type : 'POST', // Le type de la requête HTTP, ici devenu POST
         data : 'idS=' + idSponsor +'&action=' + document.getElementById('actionSponsor').value,
         dataType : 'html',
         success : function(code_html, statut)
         {
          Materialize.toast(code_html, 2000);
          update();
        }
      });
  document.getElementById('actionSponsor').value='ajouter';
}

//Cette fonction prend un id de juré en parametre
// et permet de charger un juré en vue de le modifier
function ModifierJure(idJure)
{
  document.getElementById('actionJure').value='recuperation';
  document.getElementById('idJure').value=idJure;
  var element = document.getElementById('listeJury');
  $.ajax({
   url : '/projet/siteWeb/traitement/traitementJure.php',
         type : 'POST', // Le type de la requête HTTP, ici devenu POST
         data : 'idM=' + idJure +'&action=' + document.getElementById('actionJure').value,
         dataType : 'html',
         success : function(code_html, statut)
         {
          var parsing = JSON.parse(code_html);
          element.selectedIndex = parsing[0].idJ;
          $('select').material_select();
          document.getElementById('nomJure').value = parsing[0].nom ;
          document.getElementById('prenomJure').value = parsing[0].prenom;
          document.getElementById('origineJure').value = parsing[0].origine;
        },
      });
  document.getElementById('actionJure').value='modifier';
}

// Cette fonction prend un id de juré en parametre
// et permet de supprimer un juré de la base de données
function SupprimerJure(idJure)
{
  document.getElementById('actionJure').value='supprimer';
  $.ajax({
   url : '/projet/siteWeb/traitement/traitementJure.php',
         type : 'POST', // Le type de la requête HTTP, ici devenu POST
         data : 'id=' + idJure +'&action=' + document.getElementById('actionJure').value,
         dataType : 'html',
         success : function(code_html, statut)
         {
          Materialize.toast(code_html, 2000);
          update();
        }
      });
  document.getElementById('actionJure').value='ajouter';
}

//Cette fonction prend un id de jury en parametre
// et permet de charger un jury en vue de le modifier
function ModifierJury(idJury)
{
  document.getElementById('actionJury').value='recuperation';
  document.getElementById('idJury').value=idJury;
  $.ajax({
   url : '/projet/siteWeb/traitement/traitementJury.php',
         type : 'POST', // Le type de la requête HTTP, ici devenu POST
         data : 'idJ=' + idJury +'&action=' + document.getElementById('actionJury').value,
         dataType : 'html',
         success : function(code_html, statut)
         {
          var parsing = JSON.parse(code_html);
          document.getElementById('identifiantJury').value = parsing[0].identifiant ;
        },
      });
  document.getElementById('actionJury').value='modifier';
}

// Cette fonction prend un id de jury en parametre
// et permet de supprimer un jury de la base de données
function SupprimerJury(idJury)
{
  document.getElementById('actionJury').value='supprimer';
  $.ajax({
   url : '/projet/siteWeb/traitement/traitementJury.php',
         type : 'POST', // Le type de la requête HTTP, ici devenu POST
         data : 'idJ=' + idJury +'&action=' + document.getElementById('actionJury').value,
         dataType : 'html',
         success : function(code_html, statut)
         {
          Materialize.toast(code_html, 2000);
          update();
        }
      });
  document.getElementById('actionJury').value='ajouter';
}


//Cette fonction prend un id de creneau en parametre
// et permet de charger un creneau en vue de le modifier
function ModifierCreneau(idCreneau)
{
  document.getElementById('actionCreneau').value='recuperation';
  document.getElementById('idCreneau').value=idCreneau;
  $.ajax({
   url : '/projet/siteWeb/traitement/traitementCreneau.php',
         type : 'POST', // Le type de la requête HTTP, ici devenu POST
         data : 'idC=' + idCreneau +'&action=' + document.getElementById('actionCreneau').value,
         dataType : 'html',
         success : function(code_html, statut)
         {
          var parsing = JSON.parse(code_html);
          document.getElementById('hdebut').value = parsing[0].hdebut ;
          document.getElementById('hfin').value = parsing[0].hfin ;
        },
      });
  document.getElementById('actionCreneau').value='modifier';
}

// Cette fonction prend un id de creneau en parametre
// et permet de supprimer un creneau de la base de données
function SupprimerCreneau(idCreneau)
{
  document.getElementById('actionCreneau').value='supprimer';
  $.ajax({
   url : '/projet/siteWeb/traitement/traitementCreneau.php',
         type : 'POST', // Le type de la requête HTTP, ici devenu POST
         data : 'idC=' + idCreneau +'&action=' + document.getElementById('actionCreneau').value,
         dataType : 'html',
         success : function(code_html, statut)
         {
          Materialize.toast(code_html, 2000);
          update();
        }
      });
  document.getElementById('actionCreneau').value='ajouter';
}
</script>
<?php include 'footer.php'; ?>
</body>
</html>
