<?php session_start(); ?>
<html>
  <head>
    <title>Résultats - OSI</title>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="css/materialize.min.css">
    <link rel="stylesheet" type="text/css" href="css/materialize.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="icon" type="icon" href="../appAndroid/OlympiadeSI/app/src/main/res/drawable/launcher_icon.png">
  </head>

  <body>
    <?php include 'nav.php'; ?>
    <div id="wrap">
      <div id="main">
        <h3 id="titre"><b>Résultats</b></h3>
        <img id="spsrPrincipal" src="" alt="Sponsor principal">
        <div class="middle">
          <div id="tabs" class="tableau">
            <!-- Ici on met le tableau des groupes -->
          </div>
        </div>
      </div>
      <div class="sponsor-img">
        <img id="spsrSecondaire" src="" alt="Sponsors de l'année">
      </div>
    </div>

    <?php include 'footer.php'; ?>

    <script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="js/sweetalert2.min.js"></script>
    <script type="text/javascript" src="js/materialize.min.js"></script>

    <script>
          $(document).ready(function(){
            /*
             * Quand le document charge, on charge le tableau de résultats et les
             * logos des partenaires.
             */
            update()
            addSponsor("Primaire");
            addSponsor("Secondaire");
            $('select').material_select();
          });


          function addSponsor(typeSpsr){
            /*
             * Permet d'assigner la source aux images de logos.
             * @param {String} typeSpsr - le type d'identifiant. Soit "Primaire" soit "Secondaire".
             */

            $.ajax({
              url : '/projet/siteWeb/traitement/traitementResultat.php',
              type : 'POST',
              data   : 'type='+typeSpsr + '&action=sponsor' ,
              dataType : 'html',
              success : function(code_html, statut)
              {
                let parsing = JSON.parse(code_html);
                if (typeSpsr == "Primaire") {
                  let path = "../appAndroid/OlympiadeSI/app/src/main/res/drawable/"+parsing[0].image;
                   $("#spsrPrincipal").attr("src",path);
                 }
                else {
                  let path = "../appAndroid/OlympiadeSI/app/src/main/assets/images/"+parsing[0].image;
                  $("#spsrSecondaire").attr("src",path);
                }
              },
            });
        }

          function update(){
            /*
             * Mise à jour du tableau en fonction des groupes dans la BD
             * et des prix attribués.
             */

             $.ajax({
               url : '/projet/siteWeb/traitement/traitementResultat.php',
               type : 'POST',
               data   : 'action=recuperation',
               dataType : 'html',
               success : function(code_html, statut)
               {
                 let parsing = JSON.parse(code_html);

                 console.log(parsing);

                 let element = $('#tabs');
                 /* Création du tableau */
                 let html = "<table id='mytable' class='highlight bordered'><thead>"+
                               "<tr>"+
                                 "<th id='projet'>Projet</th>"+
                                 "<th id='lycee'>Lycée</th>"+
                                 "<th id='formation'>Formation</th>"+
                                 "<th id='moyenne' class='moyenne'>Moyenne globale</th>"+
                                 "<th id='Maitrise_Scientifique' class='Maitrise_Scientifique'>Maîtrise Scientifique</th>"+
                                 "<th id='prototype' class='prototype'>Prototype</th>"+
                                 "<th id='Demarche_Scientifique' class='Demarche_Scientifique'>Démarche Scientifique</th>"+
                                 "<th id='originalite' class='originalite'>Originalité</th>"+
                                 "<th id='pluridisciplinarite' class='pluridisciplinarite'>Pluridisciplinarité</th>"+
                                 "<th id='Communication' class='Communication'>Communication</th>"+
                                 "<th id='Prix' class=''>Prix</th>"+
                                 "<th id='' class='actionTableau'>Action</th>"+
                               "</tr>"+
                             "</thead><tbody>";

                 for(let projet in parsing) {
                   /*
                    * Pour chacun des projets, on remplit la ligne avec les informations.
                    */

                   let nomP = parsing[projet].nomP;
                   /*
                    * On rend la ligne de la couleur du prix attribué (donc blanc
                    * s'il n'y a pas de prix).
                    */

                   html += "<tr class=\""+parsing[projet].nomPrix+"\">";
                   html += "<td id='nomP'>" + nomP + "</td>";

                   html += "<td id='lycee'>" + parsing[projet].lycee + "</td>";

                   html += "<td id='formation'>" + parsing[projet].formation + "</td>";

                   /* Ajout des notes */
                     html += "<td class=''";
                     html += "'>" + parseFloat(parsing[projet].moyenne).toFixed(2) + "</td>";

                     html += "<td class=''";
                     html += "'>" + parseFloat(parsing[projet].Maitrise_Scientifique).toFixed(2) + "</td>";

                     html += "<td class=''";
                     html += "'>" + parseFloat(parsing[projet].prototype).toFixed(2) + "</td>";

                     html += "<td class=''";
                     html += "'>" + parseFloat(parsing[projet].Demarche_Scientifique).toFixed(2) + "</td>";

                     html += "<td class=''";
                     html += "'>" + parseFloat(parsing[projet].originalite).toFixed(2) + "</td>";

                     html += "<td class=''";
                     html += "'>" + parseFloat(parsing[projet].pluridisciplinarite).toFixed(2) + "</td>";

                     html += "<td class=''";
                     html += "'>" + parseFloat(parsing[projet].Communication).toFixed(2) + "</td>";

                     /* Ajout du prix du groupe. */
                     html += "<td class=''";
                     if (parsing[projet].nomPrix != null)
                     html += "'>" + capitalizeFirstLetter(parsing[projet].nomPrix.split("_").join(" ")) + "</td>";
                     else
                     html += "'>-</td>";

                     /* Bouton d'action */
                     html += "<td>"+
                               "<div>"+
                                 "<a class='dropdown-button btn' href='#' data-activates='dropdown"+projet+"'> Récompenser </a>"+

                                 "<ul id='dropdown"+projet+"' class='dropdown-content'>"+
                                     "<li><a onclick=\"assigner_prix('"+ nomP +"','"+parsing[projet].nomPrix+"','general')\">"+
                                                 "<i class='material-icons'>show_chart</i>Moyenne globale 1</a></li>"+

                                     "<li><a onclick=\"assigner_prix('"+ nomP +"','"+parsing[projet].nomPrix+"','general2')\">"+
                                                 "<i class='material-icons'>show_chart</i>Moyenne globale 2</a></li>"+

                                     "<li><a onclick=\"assigner_prix('"+ nomP +"','"+parsing[projet].nomPrix+"','Maitrise_Scientifique')\">"+
                                                 "<i class='material-icons'>extension</i>Maîtrise Scientifique</a></li>"+

                                     "<li><a onclick=\"assigner_prix('"+ nomP +"','"+parsing[projet].nomPrix+"','prototype')\">"+
                                                 "<i class='material-icons'>settings</i>Prototype</a></li>"+

                                     "<li><a onclick=\"assigner_prix('"+ nomP +"','"+parsing[projet].nomPrix+"','Demarche_Scientifique')\">"+
                                                 "<i class='material-icons'>device_hub</i>Démarche Scientifique</a></li>"+

                                     "<li><a onclick=\"assigner_prix('"+ nomP +"','"+parsing[projet].nomPrix+"','originalite')\">"+
                                                 "<i class='material-icons'>whatshot</i>Originalité</a></li>"+

                                     "<li><a onclick=\"assigner_prix('"+ nomP +"','"+parsing[projet].nomPrix+"','pluridisciplinarite')\">"+
                                                 "<i class='material-icons'>settings_input_component</i>Pluridisciplinarité</a></li>"+

                                     "<li><a onclick=\"assigner_prix('"+ nomP +"','"+parsing[projet].nomPrix+"','Communication')\">"+
                                                 "<i class='material-icons'>nature_people</i>Communication</a></li>"+

                                     "<li><a onclick=\"suppression('"+parsing[projet].nomPrix+"')\">"+
                                                 "<i class='material-icons'>delete_forever</i>Supprimer prix</a></li>"+

                                   "</ul>"+
                                 "</div>"+
                               "</td>";
                   html += "</tr>";
                 }
                 html += "</tbody></table>";
                 element.html(html);

                 function capitalizeFirstLetter(string) {
                   /*
                    * Retourne une version propre des données de la base de données
                    */
                     return string.charAt(0).toUpperCase() + string.slice(1);
                 }

                 $('.dropdown-button').dropdown({
                   /* Action du bouton "Récompenser" */

                   inDuration: 300,
                   outDuration: 225,
                   constrainWidth: false, // Does not change width of dropdown to that of the activator
                   hover: false, // Activate on hover
                   gutter: 0, // Spacing from edge
                   belowOrigin: true, // Displays dropdown below the button
                   alignment: 'right', // Displays dropdown with edge aligned to the left of button
                   stopPropagation: true // Stops event propagation
                 });

                 /* Fonctions de tri de tableau */
                 $("#projet").click(function(){
                   f_projet *= -1;
                   let n = $(this).prevAll().length;
                   sortTable(f_projet,n);
                 });
                 $("#lycee").click(function(){
                   f_lycee *= -1;
                   let n = $(this).prevAll().length;
                   sortTable(f_lycee,n);
                 });
                 $("#formation").click(function(){
                   f_formation *= -1;
                   let n = $(this).prevAll().length;
                   sortTable(f_formation,n);
                 });
                 $("#moyenne").click(function(){
                   f_moyenne *= -1;
                   let n = $(this).prevAll().length;
                   sortTable(f_moyenne,n);
                 });
                 $("#Maitrise_Scientifique").click(function(){
                   let n = $(this).prevAll().length;
                   f_Maitrise_Scientifique *= -1;
                   sortTable(f_Maitrise_Scientifique,n);
                 });
                 $("#prototype").click(function(){
                   let n = $(this).prevAll().length;
                   f_prototype *= -1;
                   sortTable(f_prototype,n);
                 });
                 $("#Demarche_Scientifique").click(function(){
                   f_Demarche_Scientifique *= -1;
                   let n = $(this).prevAll().length;
                   sortTable(f_Demarche_Scientifique,n);
                 });
                 $("#originalite").click(function(){
                   f_originalite *= -1;
                   let n = $(this).prevAll().length;
                   sortTable(f_originalite,n);
                 });
                 $("#pluridisciplinarite").click(function(){
                   let n = $(this).prevAll().length;
                   f_pluridisciplinarite *= -1;
                   sortTable(f_pluridisciplinarite,n);
                 });
                 $("#Communication").click(function(){
                   f_Communication *= -1;
                   let n = $(this).prevAll().length;
                   sortTable(f_Communication,n);
                 });
                 $("#Prix").click(function(){
                   f_Prix *= -1;
                   let n = $(this).prevAll().length;
                   sortTable(f_Prix,n);
                 });
               },
       });
     }


    function suppression(critere) {
      /* Suppression du prix
       * @param {String} critere - Prix à supprimer
       */

      $.ajax({
        url : '/projet/siteWeb/traitement/traitementResultat.php',
        type : 'POST',
        data : '&critere=' + critere + '&action=suppression',
        dataType : 'html',
        success : function(code_html, statut)
        {
          update();
        },
      });
    }

    function assigner_prix(nomP, prix, critere){
      /*
       * Permet d'assigner un prix à un projet.
       * @param {String} nomP    - Nom du projet à récompenser.
       * @param {String} prix    - Prix actuel du projet. "null" si pas de récompense actuelle
       * @param {String} critere - Prix à attribuer au groupe nomP
       */

       /* Si le projet a déjà un prix, on le supprime */
       if (prix != "null") { suppression(prix); }
       /* On supprime le lien entre le nouveau prix et l'ancien groupe à qui il était attribué */
       suppression(critere);

      $.ajax({
        url : '/projet/siteWeb/traitement/traitementResultat.php',
        type : 'POST',
        data : 'nomP=' + nomP + '&critere=' + critere + '&action=assigner_prix',
        dataType : 'html',
        success : function(code_html, statut)
        {
          /* Si l'assignation fonctionne, on update*/
          console.warn(code_html);
          if (code_html.includes("assignation réussi")) {
            update();
          /* Si deux groupes du même lycée sont déjà récompensés, alert d'erreur */
          } else if (code_html.includes("assignation ratée car plus de 2 groupes viennent du meme lycée")){
            swal("Assignation impossible","Impossible de récompenser le groupe "+ nomP + " car deux autres groupes sont déjà récompensés dans le même lycée."
            + " Veuillez supprimer le prix d'un de ces groupes avant de récompenser celui-ci.", "error");
          /* Si deux groupes du même lycée avec le prix général, alert d'erreur */
          } else {
            swal("Assignation impossible","Impossible de récompenser le groupe "+ nomP + " dans le critère "+ critere +" un des prix généraux est déjà attribué dans ce lycée.", "error");
          }
        },
        error : function(resultat, statut, erreur){
          console.warn('ERREUR !!    resultat: '+resultat+'      statut: '+statut+'     erreur: '+erreur);
        }
      });
    }
    </script>


    <script type="text/javascript">

      function sortTable(f,n){
        /* Fonction de tri du tableau */
        let rows = $('#mytable tbody tr').get();

        rows.sort(function(a, b){
          let A = getVal(a);
          let B = getVal(b);
          if(A < B) {return -1*f;}
          if(A > B) {return  1*f;}
          return 0;
        });

        function getVal(elm){
          let v = $(elm).children('td').eq(n).text().toUpperCase();
          if($.isNumeric(v)){v = parseFloat(v,10);}
          return v;
        }

        $.each(rows, function(index, row) {
          $('#mytable').children('tbody').append(row);
        });
      }

      let f_projet = 1;
      let f_lycee = 1;
      let f_formation = 1;
      let f_moyenne = 1;
      let f_Maitrise_Scientifique = 1;
      let f_prototype = 1;
      let f_Demarche_Scientifique = 1;
      let f_originalite = 1;
      let f_pluridisciplinarite = 1;
      let f_Communication = 1;
      let f_Prix = 1;
      </script>
  </body>
</html>
