<?php session_start();
?>
<html>
  <head>
    <title>Planning Admin - OSI</title>
    <meta charset="utf-8">
    <link rel="icon" type="icon" href="../appAndroid/OlympiadeSI/app/src/main/res/drawable/launcher_icon.png">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel='stylesheet' type="text/css" href='css/jquery-ui.min.css' />
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel='stylesheet' type="text/css" href='css/fullcalendar.min.css' />
    <link rel='stylesheet' type="text/css" href='css/scheduler.min.css' />
    <link rel="stylesheet" type="text/css" href="css/materialize.min.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="icon" type="icon" href="../appAndroid/OlympiadeSI/app/src/main/res/drawable/launcher_icon.png">


    <script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="js/materialize.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src='js/moment.min.js'></script>
    <script type="text/javascript" src='js/fullcalendar.min.js'></script>
    <script type="text/javascript" src='js/scheduler.min.js'></script>
    <script type="text/javascript" src="js/sweetalert2.min.js"></script>


  </head>

  <body>
    <?php include 'nav.php'; ?>
    <div id="wrap">
      <div id="main" class="container">
        <div id="Ptitre">
          <h3 id="titre">Planning jury</h3>
          <img id="spsrPrincipal" src="" alt="Sponsor principal">
        </div>
        <div id='calendar'></div>
        <div class="sponsor-img">
          <img id="spsrSecondaire" src="" alt="Sponsors de l'année">
        </div>
      </div>
    </div>
      <script type="text/javascript">
        $(document).ready(function() {
            $('select').material_select();
            addSponsor("Primaire");
            addSponsor("Secondaire");
            creationPlanning();
        });

        // fonction qui génère le planning grace au framework
        // fonction asynchrone afin de pouvoir attendre les reponses des requetes ajax nessessaires a la creation du planning

        async function creationPlanning(){
          let listeCreneau = await setCreneau();
          listeCreneau = JSON.parse(listeCreneau);
          let creneau = initCreneau(listeCreneau);
          var eventOrigin;
          $('#calendar').fullCalendar({
            schedulerLicenseKey: 'CC-Attribution-NonCommercial-NoDerivatives',
            now: "2012-12-12",
            defaultView: 'agendaDay',
            theme: true,
            themeSystem: 'jquery-ui',
            footer: false,
            eventLimit: true,
            selectable: false,
            editable: false,
            droppable: false,
            eventDurationEditable: false,
            allDaySlot: false,
            contentHeight: "auto",
            nowIndicator: true,

            slotDuration: creneau['time'],
            minTime: creneau['debut'],
            maxTime: creneau['fin'],
            slotLabelFormat: "H(:mm)",


            header: {
              left: '',
              center: '',
              right: ''
            },

            // Charge les jurys et les affiche dans le planning

            resources: function(callback) {
              $.ajax({
                 url: 'traitement/traitementplanning.php',
                 type : 'POST',
                 data : 'action=jury',
                 dataType : 'html',
                 success : function(code_html, statut){
                   var parsing = JSON.parse(code_html);
                   var res = [];
                   for (var e in parsing){
                     res.push({id: parsing[e]['identifiant'], title: 'jury '+parsing[e]['identifiant']});
                   }
                   let taille = 180*res.length;
                   let tDiv = document.getElementById("main").offsetWidth - 40;
                   taille<tDiv?$('.fc-view-container .fc-view > table').css("width",String(tDiv)+"px"):$('.fc-view-container .fc-view > table').css("width",String(taille)+"px");
                   callback(res);
                 },
                 error : function(resultat, statut, erreur){
                   swal("","Erreur de chargement des jurys", "error");
                 }
              });
            },

            // Charge les evenements déjà presents dans la BD

            events: function(start, end, timezone, callback){
              $.ajax({
                 url: 'traitement/traitementplanning.php',
                 type : 'POST',
                 data : 'action=recuperation',
                 dataType : 'html',
                 success : function(code_html, statut){
                   var parsing = JSON.parse(code_html);
                   var res = [];
                   for (var e in parsing){
                     var debut = "2012-12-12 "+parsing[e]["hdebut"];
                     var fin = "2012-12-12 "+parsing[e]["hfin"];
                     res.push({resourceId: parsing[e]["identifiant"], start: debut, end: fin, nomP:parsing[e]["nomP"], title: parsing[e]["nomP"]+"\nLycée : "+parsing[e]["lycee"]+"\nEmplacement n° "+parsing[e]["emplacement"],customRender: true,etat:parsing[e]["etat"]});
                   }
                   callback(res);
                 },
                 error : function(resultat, statut, erreur){
                   swal("","Erreur de chargement des événements", "error");
                 }
              });
            },

            // fonction trigger lors du passage de la souris sur un evenement

            eventMouseover: function( event, jsEvent, view ) {
              $(this).css('background-color', '#3a87ad'); //#77B5FE
            },

            // fonction trigger lors de la sortie de la souris d'un evenement

            eventMouseout: function( event, jsEvent, view ) {
              $(this).css('background-color', '#22427C');
            },

            // fonction trigger lors de l'affichage d'un evenement

            eventRender: function(event, element) {
              var el = element.html();
              switch(event['etat']){
                case "attente" : element.html("<div style='width:90%;float:left;'>" +  el + "</div><div style='text-align:right;' ><svg width='30' height='30' >  <rect width='30' height='30' style='fill:rgb(255,127,0);stroke-width:3;stroke:rgb(0,0,0)' />/svg> </div>"); break;
                case "absent" : element.html("<div style='width:90%;float:left;'>" +  el + "</div><div style='text-align:right;' ><svg width='30' height='30' >  <rect width='30' height='30' style='fill:rgb(255,0,0);stroke-width:3;stroke:rgb(0,0,0)' />/svg> </div>"); break;
                case "valide" : element.html("<div style='width:90%;float:left;'>" +  el + "</div><div style='text-align:right;' ><svg width='30' height='30' >  <rect width='30' height='30' style='fill:rgb(0,255,0);stroke-width:3;stroke:rgb(0,0,0)' />/svg> </div>"); break;
                default: element.html(el);
              }
            }
          });
        }

        // fonction qui initialise la variable creneau

        function initCreneau(l){
          c = {};
          c['debut'] = l[0]['hdebut'];
          c['fin'] = l[Object.keys(l).length-1]['hfin'];

          c['time'] = "00:10:00";
          return c;
        }

        // fonction qui fait une requete ajax pour recuperer tous les creneaux

        function setCreneau(callback){
          return $.ajax({
             url: 'traitement/traitementplanning.php',
             type : 'POST',
             data : 'action=creneau',
             dataType : 'html',
             success : function(code_html, statut){
               let parsing = JSON.parse(code_html);
               let listeCreneau = [];
               for (var e in parsing){
                 listeCreneau.push({hdebut: parsing[e]['hdebut'], hfin: parsing[e]['hfin']});
               }
             },
             error : function(resultat, statut, erreur){
               swal("","Erreur de chargement des créneaux disponible", "error");
             }
          });
        }

        // fonction qui fait une requete ajax afin de recuperer les liens des images des sponsors

        function addSponsor(typeSpsr){
            $.ajax({
              url : '/projet/siteWeb/traitement/traitementResultat.php',
              type : 'POST',
              data   : 'type='+typeSpsr + '&action=sponsor' ,
              dataType : 'html',
              success : function(code_html, statut){
                let parsing = JSON.parse(code_html);

                if (typeSpsr == "Primaire") {
                  let path = "../appAndroid/OlympiadeSI/app/src/main/res/drawable/"+parsing[0].image;
                   $("#spsrPrincipal").attr("src",path);
                 }
                else {
                  let path = "../appAndroid/OlympiadeSI/app/src/main/assets/images/"+parsing[0].image;
                  $("#spsrSecondaire").attr("src",path);
                }
              },
              error : function(resultat, statut, erreur){
                swal("","Erreur de chargement des sponsor", "error");
              }
            });
        }

        </script>

  <?php include 'footer.php'?>
  </body>
</html>
