
create table PERSONNE
(
  id int,
  nom varchar(50),
  prenom varchar(50),
  PRIMARY KEY(id)
);

create table PROJET
(
  idP int,
  nomP varchar(50),
  description varchar(500),
  image varchar(100),
  emplacement varchar(5),
  PRIMARY KEY(idP),
  CONSTRAINT projet1 unique (nomP)
);

create table JURY
(
  idJ int,
  identifiant varchar (20),
  mot_de_passe varchar(56),
  CONSTRAINT projet1 unique (identifiant),
  PRIMARY KEY(idJ)
);

create table ETUDIANT
(
  idE int,
  lycee varchar(50),
  formation varchar(50),
  idP int,
  CONSTRAINT etu1 FOREIGN KEY (idE) REFERENCES PERSONNE(id),
  CONSTRAINT etu2 FOREIGN KEY (idP) REFERENCES PROJET(idP),
  PRIMARY KEY (idE)
);

create table MEMBRE_DU_JURY
(
  idM int,
  origine varchar(50),
  idJ int,
  CONSTRAINT jure1 FOREIGN KEY (idM) REFERENCES PERSONNE(id),
  CONSTRAINT jure2 FOREIGN KEY (idJ) REFERENCES JURY(idJ),
  PRIMARY KEY(idM)
);

create table CRENEAU
(
  idC int,
  hdebut time,
  hfin time,
  PRIMARY KEY (idC)
);

create table SPONSOR
(
  idS int,
  nom varchar(50),
  image varchar(100),
  type varchar(10),
  annee int,
  PRIMARY KEY(ids)
);

create table NOTES
(
  idJ int,
  idP int,
  originalite int,
  prototype int,
  Demarche_Scientifique int,
  pluridisciplinarite int,
  Maitrise_Scientifique int,
  Communication int,
  etat varchar(15),
  CONSTRAINT notes1 FOREIGN KEY (idJ) REFERENCES JURY(idJ),
  CONSTRAINT notes2 FOREIGN KEY (idP) REFERENCES PROJET(idP),
  PRIMARY KEY(idJ,idP)
);

create table JUGE
(
  idJ int,
  idP int,
  idC int,
  CONSTRAINT juge1 FOREIGN KEY (idJ) REFERENCES JURY(idJ),
  CONSTRAINT juge2 FOREIGN KEY (idP) REFERENCES PROJET(idP),
  CONSTRAINT juge3 FOREIGN KEY (idC) REFERENCES CRENEAU(idC),
  PRIMARY KEY(idJ,idP,idC),
  CONSTRAINT juge4 unique (idJ,idC),
  CONSTRAINT juge5 unique (idJ,idP),
  CONSTRAINT juge6 unique (idP,idC)
);

create table PRIX
(
  idP int,
  nomPrix varchar(100),
  ordre int,
  assigne boolean,
  PRIMARY KEY(ordre),
  CONSTRAINT prix2 FOREIGN KEY (idP) REFERENCES PROJET(idP)
);
