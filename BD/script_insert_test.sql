﻿insert into PERSONNE (id, nom, prenom) values (1,'Poney','Billy');
insert into PERSONNE (id, nom, prenom) values (2,'Wololo','Gontran');
insert into PERSONNE (id, nom, prenom) values (3,'Castellanos','Sebastian');
insert into PERSONNE (id, nom, prenom) values (4,'Deloin','Alain');
insert into PERSONNE (id, nom, prenom) values (5,'Moissay','Bobby');
insert into PERSONNE (id, nom, prenom) values (6,'Ptitegoutte','Emma');
insert into PERSONNE (id, nom, prenom) values (7,'Ptitegoutte','Justine');
insert into PERSONNE (id, nom, prenom) values (8,'Ptitegoutte','Corinne');
insert into PERSONNE (id, nom, prenom) values (9,'Ptitegoutte','Melusine');
insert into PERSONNE (id, nom, prenom) values (10,'Bonbeur','Jean');

insert into PROJET (idp,nomP,description,image,emplacement) values (1,'REAL JDR','Quam ob rem circumspecta cautela observatum est deinceps et cum edita montium petere coeperint grassatores, loci iniquitati milites cedunt. ubi autem in planitie potuerint reperiri, quod contingit adsidue, nec exsertare lacertos nec crispare permissi tela, quae vehunt bina vel terna, pecudum ritu inertium trucidantur.','test.png',2);
insert into PROJET values (2,'TAVERN','Omni adultam et ingressus discessit erectus ingressus in extremum trecentis circummurana ambit vitae extremum vergens bella populus reportavit circumcluditur aetatem fere aerumnas circummurana fretum tempus senium iuvenem virum adultam pueritiae','test2.png',1);
insert into PROJET (idp,nomP,description,image,emplacement) values (0,' ','Ce projet est fictif et ne sert que pour le bon fonctionnement de la BD','',0);
insert into PROJET (idp,nomP,description,image,emplacement) values (3,'test1',"ceci est pour tester","test.png",0);
insert into PROJET (idp,nomP,description,image,emplacement) values (4,'test2',"ceci est pour tester","test.png",0);
insert into PROJET (idp,nomP,description,image,emplacement) values (5,'test3',"ceci est pour tester","test.png",0);
insert into PROJET (idp,nomP,description,image,emplacement) values (6,'test4',"ceci est pour tester","test.png",0);


insert into JURY (idJ,identifiant,mot_de_passe) values (-1,'admin','58acb7acccce58ffa8b953b12b5a7702bd42dae441c1ad85057fa70b1');
insert into JURY (idJ,identifiant,mot_de_passe) values (1,'1','4b59e8216b8799816c3766587207846522f59f400c74ee0d0a68a45e');
insert into JURY (idJ,identifiant,mot_de_passe) values (2,'2','4b59e8216b8799816c3766587207846522f59f400c74ee0d0a68a45e');
insert into JURY (idJ,identifiant,mot_de_passe) values (3,'3','4b59e8216b8799816c3766587207846522f59f400c74ee0d0a68a45e');
insert into JURY (idJ,identifiant,mot_de_passe) values (4,'4','4b59e8216b8799816c3766587207846522f59f400c74ee0d0a68a45e');
insert into JURY (idJ,identifiant,mot_de_passe) values (5,'5','4b59e8216b8799816c3766587207846522f59f400c74ee0d0a68a45e');
insert into JURY (idJ,identifiant,mot_de_passe) values (0,' ','FAKE jury');

insert into ETUDIANT (idE,lycee,formation,idP) values (1,'Fitz','STI2D',1);
insert into ETUDIANT (idE,lycee,formation,idP) values (2,'Fitz','SI',3);
insert into ETUDIANT (idE,lycee,formation,idP) values (6,'Paul Ricard','SI',2);
insert into ETUDIANT (idE,lycee,formation,idP) values (7,'Paul Ricard','SI',4);
insert into ETUDIANT (idE,lycee,formation,idP) values (8,'Paul Ricard','SI',5);
insert into ETUDIANT (idE,lycee,formation,idP) values (9,'Paul Ricard','SI',6);

insert into MEMBRE_DU_JURY (idM,origine,idJ) values (3,'Hopital de ton coeur',1);
insert into MEMBRE_DU_JURY (idM,origine,idJ) values (4,'Cinema français',1);
insert into MEMBRE_DU_JURY (idM,origine,idJ) values (5,'WTC',2);
insert into MEMBRE_DU_JURY (idM,origine,idJ) values (10,'Creuse',2);

insert into CRENEAU (idC,hdebut,hfin) values (8,'8:30:00','9:00:00');
insert into CRENEAU (idC,hdebut,hfin) values (1,'9:00:00','9:30:00');
insert into CRENEAU (idC,hdebut,hfin) values (2,'9:30:00','10:00:00');
insert into CRENEAU (idC,hdebut,hfin) values (3,'10:30:00','11:00:00');
insert into CRENEAU (idC,hdebut,hfin) values (4,'11:00:00','11:30:00');
insert into CRENEAU (idC,hdebut,hfin) values (5,'11:30:00','12:00:00');
insert into CRENEAU (idC,hdebut,hfin) values (6,'12:00:00','12:30:00');
insert into CRENEAU (idC,hdebut,hfin) values (7,'10:00:00','10:30:00');

insert into NOTES (idJ,idP,originalite,prototype,Demarche_Scientifique,pluridisciplinarite,Maitrise_Scientifique,Communication,etat) values (1,1,1,2,3,4,5,6,"attente");
insert into NOTES (idJ,idP,originalite,prototype,Demarche_Scientifique,pluridisciplinarite,Maitrise_Scientifique,Communication,etat) values (2,1,1,5,4,3,2,1,"valide");
insert into NOTES (idJ,idP,originalite,prototype,Demarche_Scientifique,pluridisciplinarite,Maitrise_Scientifique,Communication,etat) values (2,2,1,2,3,4,5,6,"absent");
insert into NOTES (idJ,idP,originalite,prototype,Demarche_Scientifique,pluridisciplinarite,Maitrise_Scientifique,Communication,etat) values (1,2,6,5,4,3,2,1,"attente");

insert into JUGE (idJ,idP,idC) values (1,1,1);
insert into JUGE (idJ,idP,idC) values (2,1,2);
insert into JUGE (idJ,idP,idC) values (1,2,2);
insert into JUGE (idJ,idP,idC) values (2,2,1);

insert into PRIX (idP,nomPrix,ordre,assigne) values (0,'general',1,False);
insert into PRIX (idP,nomPrix,ordre,assigne) values (0,'general2',2,False);
insert into PRIX (idP,nomPrix,ordre,assigne) values (0,'Maitrise_Scientifique',3,False);
insert into PRIX (idP,nomPrix,ordre,assigne) values (0,'prototype',4,False);
insert into PRIX (idP,nomPrix,ordre,assigne) values (0,'Demarche_Scientifique',5,False);
insert into PRIX (idP,nomPrix,ordre,assigne) values (0,'originalite',6,False);
insert into PRIX (idP,nomPrix,ordre,assigne) values (0,'pluridisciplinarite',7,False);
insert into PRIX (idP,nomPrix,ordre,assigne) values (0,'Communication',8,False);

insert into SPONSOR (idS,nom,image,annee) values (1,'EDF','logo.png',2018);
insert into SPONSOR (idS,nom,image,annee) values (2,'Divers','logo2.png',2018);
