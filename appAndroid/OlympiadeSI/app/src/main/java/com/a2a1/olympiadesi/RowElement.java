package com.a2a1.olympiadesi;

import android.content.Context;
import android.graphics.Color;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.JSON.JSONSystem;

/**
 * Classe qui représente un élément de groupe et ses informations sur la vue ScheduleActivity
 * @author Antoine Tamin
 */
public class RowElement {

    private String nameGroup, startHours, endHours, state;

    private LinearLayout.LayoutParams lpMain, lpHours, lpHoursMargin, lpState;
    private LinearLayout linearLayout, layoutHours;

    private TextView startHour, endHour;

    private Button buttonGroup, buttonState;

    private int idProjet;

    private Context context;


    /**
     * Constructeur de la classe
     * @param context contexte de l'activité dans laquelle on appelle cette le constructeur de
     * la classe
     */
    public RowElement(Context context) {
        this.context = context;
    }


    /**
     * Création d'un élément et positionnement sur la vue
     * @param i indice du groupe dans le fichier JSON
     */
    public void createRowElement(int i) {

        /*
         * Récupération des informations du groupe dans le fichier JSON avec la clée et l'indice
         * du groupe
         */

        nameGroup = (String) JSONSystem.getValueInGroupes("nom", i);
        startHours = ((String) JSONSystem.getValueInGroupes("horaireDeb", i)).substring(0,5);
        endHours = ((String) JSONSystem.getValueInGroupes("horaireFin", i)).substring(0,5);
        state = (String) JSONSystem.getValueInGroupes("etat", i);
        idProjet = Integer.valueOf((String) JSONSystem.getValueInGroupes("numéro", i));

        /*
         * Layout principal qui contiendra l'élément créé et positionnement sur la vue
         */

        lpMain = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lpMain.setMargins(0, 30, 0, 0);
        lpMain.gravity = Gravity.CENTER;
        linearLayout = new LinearLayout(context);
        linearLayout.setOrientation(LinearLayout.HORIZONTAL);
        linearLayout.setLayoutParams(lpMain);

        /*
         * Layout des heures du groupe et positionnement sur la vue
         */

        layoutHours = new LinearLayout(context);
        layoutHours.setOrientation(LinearLayout.VERTICAL);
        lpHours = new LinearLayout.LayoutParams(55, ViewGroup.LayoutParams.WRAP_CONTENT);
        lpHours.setMarginEnd(convDpToPx(20));
        layoutHours.setLayoutParams(lpHours);

        /*
         * Champs où sont affichées les heures
         */

        startHour = new TextView(context);
        endHour = new TextView(context);

        /*
         * Paramètre du layout des heures
         */

        lpHoursMargin = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lpHoursMargin.setMargins(0, 65, 0, 0);
        lpHoursMargin.gravity = Gravity.END;
        endHour.setLayoutParams(lpHoursMargin);

        /*
         * On attribue les heures récupérées en JSON aux champs correspondants
         */

        startHour.setText(" " + startHours);
        endHour.setText(endHours);

        /*
         * On change la couleur des heures
         */

        startHour.setTextColor(Color.parseColor("#1c2324"));
        endHour.setTextColor(Color.parseColor("#1c2324"));

        /*
         * Ajout des heures au layout qui doit les contenir
         */

        layoutHours.addView(startHour);
        layoutHours.addView(endHour);

        /*
         * Bouton qui affiche le nom du groupe et placement sur la vue
         */

        buttonGroup = new Button(context);
        buttonGroup.setText(nameGroup);
        buttonGroup.setBackgroundColor(Color.parseColor("#4080ff"));
        buttonGroup.setWidth(convDpToPx(300));
        buttonGroup.setHeight(convDpToPx(90));
        buttonGroup.setTextColor(Color.WHITE);
        buttonGroup.setTextSize(30);
        buttonGroup.setPadding(30,0 , 0, 0);
        buttonGroup.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_group_white_24px, 0, 0, 0);

        /*
         * Layout correspondant au bouton d'état du groupe et placement sur la vue
         */

        lpState = new LinearLayout.LayoutParams(convDpToPx(30), convDpToPx(30));
        lpState.setMarginStart(convDpToPx(45));
        lpState.gravity = Gravity.CENTER;

        buttonState = new Button(context);

        /*
         * Attribution de la couleur du bouton d'état suivant l'information récupérée dans le JSON
         */

        if (state.equals("valide")) {
            buttonState.setBackgroundColor(Color.parseColor("#4caf50"));
        }

        else if (state.equals("attente")) {
            buttonState.setBackgroundColor(Color.parseColor("#ff9800"));
        }

        else {
            buttonState.setBackgroundColor(Color.parseColor("#c0341d"));
        }

        /*
         * Ajout des paramètres du bouton au bouton d'état
         */

        buttonState.setLayoutParams(lpState);

        /*
         * AJout du layout des heures, du bouton du groupe et du bouton d'état au layout principal
         */

        linearLayout.addView(layoutHours);
        linearLayout.addView(buttonGroup);
        linearLayout.addView(buttonState);
    }


    /**
     * Conversion de dp en pixel
     * @param dp valeur dp à convertir
     * @return dp converti en pixel
     */
    public int convDpToPx(float dp) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, metrics);
    }


    /**
     * Renvoie le nom du groupe
     * @return le string correspondant au nom du groupe
     */
    public String getNameGroup() {
        return nameGroup;
    }


    /**
     * Renvoie du bouton d'état du groupe
     * @return le bouton correspondant à l'état du groupe
     */
    public Button getButtonState() {
        return buttonState;
    }


    /**
     * Attribut le string en paramètre à l'état actuel du groupe
     * @param etat string que l'on veut attribuer à l'état
     */
    public void setState(String etat) {
        state = etat;
    }


    /**
     * Renvoie le Layout principal
     * @return le layout principal qui contient le groupe et ses informations
     */
    public LinearLayout getLinearLayout() {
        return linearLayout;
    }


    /**
     * Renvoie le bouton du groupe
     * @return le bouton du groupe qui permet d'accéder à sa vue de notation
     */
    public Button getButtonGroup() {
        return buttonGroup;
    }


    /**
     * Renvoie l'id du projet
     * @return l'id du projet récupéré en JSON
     */
    public int getIdProjet() {
        return idProjet;
    }


    /**
     * Renvoie l'heure de fin de notation du groupe
     * @return l'heure de fin du créneau durant lequel le groupe est évalué
     */
    public String getEndHours() {
        return endHours;
    }
}
