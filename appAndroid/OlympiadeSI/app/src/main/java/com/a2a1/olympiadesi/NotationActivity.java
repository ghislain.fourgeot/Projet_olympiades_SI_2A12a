package com.a2a1.olympiadesi;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.JDBC.Notes;
import com.JSON.JSONSystem;

import org.json.JSONException;
import org.w3c.dom.Text;

import java.io.IOException;
import java.io.InputStream;

import br.com.felix.imagezoom.ImageZoom;

/**
 * Classe qui correspond à l'activité de notation
 * @author Maxime Coucke
 * @see AppCompatActivity
 */

public class NotationActivity extends AppCompatActivity {

    private IntentFilter filter;
    private BroadcastReceiver mReceiver;

    private SeekBar seekBar1;
    private SeekBar seekBar2;
    private SeekBar seekBar3;
    private SeekBar seekBar4;
    private SeekBar seekBar5;
    private SeekBar seekBar6;

    private TextView textView1;
    private TextView textView2;
    private TextView textView3;
    private TextView textView4;
    private TextView textView5;
    private TextView textView6;

    private TextView numJury;
    private TextView nomProjet;
    private TextView description;
    private TextView emplacement;

    private Button boutonAbsent;
    private Button boutonSave;

    private ImageZoom image;

    private TextView originaliteView;
    private TextView prototypeView;
    private TextView demarcheView;
    private TextView pluriView;
    private TextView maitriseView;
    private TextView developpementView;

    private ImageView originaliteIcon;
    private ImageView prototypeIcon;
    private ImageView demarcheIcon;
    private ImageView pluriIcon;
    private ImageView maitriseIcon;
    private ImageView developpementIcon;


    /**
     Crée la vue et assigne les listeners aux boutons et aux champs.
     @param savedInstanceState ensemble des informations concernant l'activité
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notation);

        /*
         * Filtre qui vérifie si la tablette s'éteint afin de déconnecter l'utilisateur
         */

        filter = new IntentFilter(Intent.ACTION_SCREEN_ON);
        filter.addAction(Intent.ACTION_SCREEN_OFF);
        mReceiver = new ScreenReceiver();
        registerReceiver(mReceiver, filter);


        /*
         * Branchement des différents éléments graphiques
         */

        seekBar1 = (SeekBar) findViewById(R.id.seekBar1);
        seekBar2 = (SeekBar) findViewById(R.id.seekBar2);
        seekBar3 = (SeekBar) findViewById(R.id.seekBar3);
        seekBar4 = (SeekBar) findViewById(R.id.seekBar4);
        seekBar5 = (SeekBar) findViewById(R.id.seekBar5);
        seekBar6 = (SeekBar) findViewById(R.id.seekBar6);

        textView1 = (TextView) findViewById(R.id.textView1);
        textView2 = (TextView) findViewById(R.id.textView2);
        textView3 = (TextView) findViewById(R.id.textView3);
        textView4 = (TextView) findViewById(R.id.textView4);
        textView5 = (TextView) findViewById(R.id.textView5);
        textView6 = (TextView) findViewById(R.id.textView6);

        image = findViewById(R.id.image);

        boutonSave = (Button) findViewById(R.id.boutonSave);
        boutonAbsent = (Button) findViewById(R.id.boutonAbsent);

        numJury = findViewById(R.id.viewIdJury);
        nomProjet = findViewById(R.id.nomProjet);
        description = findViewById(R.id.description);
        emplacement = findViewById(R.id.emplacement);

        originaliteView = findViewById(R.id.originalite);
        prototypeView = findViewById(R.id.prototype);
        demarcheView = findViewById(R.id.demarche);
        pluriView = findViewById(R.id.pluri);
        maitriseView = findViewById(R.id.maitrise);
        developpementView = findViewById(R.id.developpement);

        originaliteIcon = findViewById(R.id.originaliteIcon);
        prototypeIcon = findViewById(R.id.prototypeIcon);
        demarcheIcon = findViewById(R.id.demarcheIcon);
        pluriIcon = findViewById(R.id.pluriIcon);
        maitriseIcon = findViewById(R.id.maitriseIcon);
        developpementIcon = findViewById(R.id.developpementIcon);

        /*
         * Récupération de l'intent parent
         */

        Intent parentActivity = getIntent();

        /*
         * Récupération de l'index du groupe via l'intent parent
         */

        final int indexGroup = parentActivity.getIntExtra("indexGroup", 0);

        /*
         * Récupération des données du JSON grâce à l'index du groupe récupéré précédemment
         */

        final int idJury = parentActivity.getIntExtra("idJury", 0);
        final int idP = parentActivity.getIntExtra("numéro", 0);
        final String nameGroup = (String) JSONSystem.getValueInGroupes("nom", indexGroup);
        String descriptionValue = (String) JSONSystem.getValueInGroupes("description", indexGroup);
        String emplacementValue = (String) JSONSystem.getValueInGroupes("emplacement", indexGroup);
        String imageName = (String) JSONSystem.getValueInGroupes("image", indexGroup);

        int originaliteInitValue = (int) JSONSystem.getValueInGroupes("originalite", indexGroup);
        int prototypeInitValue = (int) JSONSystem.getValueInGroupes("prototype", indexGroup);
        int demarcheInitValue = (int) JSONSystem.getValueInGroupes("demarcheSI", indexGroup);
        int pluriInitValue = (int) JSONSystem.getValueInGroupes("pluridisciplinarite", indexGroup);
        int maitriseInitValue = (int) JSONSystem.getValueInGroupes("maitrise", indexGroup);
        int developpementInitValue = (int) JSONSystem.getValueInGroupes("developpement_durable", indexGroup);

        /*
         * Essaye de récupérer l'image de projet dans le dossier puis l'affiche
         */
        try {
            //Récupère le flux entrant
            InputStream ims = getAssets().open("images/"+imageName);
            //Charge l'image en tant que dessin
            Drawable d = Drawable.createFromStream(ims, null);
            //Défini l'image
            this.image.setImageDrawable(d);
        }
        catch(IOException ex){
            Log.e("I/O ERROR", ex.getMessage());
        }

        /*
         * Fixe les notes des différents critères selon les valeurs récupérées précédemment
         */
        textView1.setText("" + originaliteInitValue);
        textView2.setText("" + prototypeInitValue);
        textView3.setText("" + demarcheInitValue);
        textView4.setText("" + pluriInitValue);
        textView5.setText("" + maitriseInitValue);
        textView6.setText("" + developpementInitValue);

        /*
         * Fixe la progression des barres en fonction des notes
         */
        seekBar1.setProgress(originaliteInitValue);
        seekBar2.setProgress(prototypeInitValue);
        seekBar3.setProgress(demarcheInitValue);
        seekBar4.setProgress(pluriInitValue);
        seekBar5.setProgress(maitriseInitValue);
        seekBar6.setProgress(developpementInitValue);

        /*
         * Fixe les différents éléments de la vue en fonction du groupe selectionné
         * sur la vue précédente
         */
        numJury.setText(numJury.getText() + " " + idJury);
        description.setText(descriptionValue);
        emplacement.setText(emplacement.getText() + " : " + emplacementValue);
        nomProjet.setText(nameGroup);

        /*
         * Listener du bouton absent renvoyant les données et le statut absent
         */
        boutonAbsent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent absentView = new Intent();
                absentView.putExtra("numéro", idP);
                absentView.putExtra("idJury", idJury);
                absentView.putExtra("nameGroup", nameGroup);
                absentView.putExtra("originalite", 0);
                absentView.putExtra("prototype", 0);
                absentView.putExtra("demarcheSI", 0);
                absentView.putExtra("pluridisciplinarite", 0);
                absentView.putExtra("maitrise", 0);
                absentView.putExtra("developpement", 0);
                absentView.putExtra("etat", "absent");

                JSONSystem.setValueInGroupe("etat", indexGroup, "absent");
                JSONSystem.setValueInGroupe("originalite", indexGroup, 0);
                JSONSystem.setValueInGroupe("prototype", indexGroup, 0);
                JSONSystem.setValueInGroupe("demarcheSI", indexGroup, 0);
                JSONSystem.setValueInGroupe("pluridisciplinarite", indexGroup, 0);
                JSONSystem.setValueInGroupe("maitrise", indexGroup, 0);
                JSONSystem.setValueInGroupe("developpement_durable", indexGroup, 0);

                setResult(Activity.RESULT_OK,absentView);
                finish();
            }
        });


        /*
         * Listener du bouton sauvegarder renvoyant les données et le statut validé
         */
        boutonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent valideView = new Intent();
                valideView.putExtra("numéro", idP);
                valideView.putExtra("idJury", idJury);
                valideView.putExtra("nameGroup", nameGroup);
                valideView.putExtra("originalite", getProgressbar1());
                valideView.putExtra("prototype", getProgressbar2());
                valideView.putExtra("demarcheSI", getProgressbar3());
                valideView.putExtra("pluridisciplinarite", getProgressbar4());
                valideView.putExtra("maitrise", getProgressbar5());
                valideView.putExtra("developpement", getProgressbar6());
                valideView.putExtra("etat", "valide");

                JSONSystem.setValueInGroupe("etat", indexGroup, "valide");
                JSONSystem.setValueInGroupe("originalite", indexGroup, getProgressbar1());
                JSONSystem.setValueInGroupe("prototype", indexGroup, getProgressbar2());
                JSONSystem.setValueInGroupe("demarcheSI", indexGroup, getProgressbar3());
                JSONSystem.setValueInGroupe("pluridisciplinarite", indexGroup, getProgressbar4());
                JSONSystem.setValueInGroupe("maitrise", indexGroup, getProgressbar5());
                JSONSystem.setValueInGroupe("developpement_durable", indexGroup, getProgressbar6());

                setResult(Activity.RESULT_OK,valideView);
                finish();
            }
        });


        /*
         * Listeners des 6 barres de progression permettant de changer automatiquement la valeur
         * de la note attribuée en fonction de la progression de la barre
         */

        this.seekBar1.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int progress = 0;


            @Override
            public void onProgressChanged(SeekBar seekBar, int progressValue, boolean fromUser) {
                progress = progressValue;
                textView1.setText("" + progress);

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }

        });

        this.seekBar2.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int progress = 0;


            @Override
            public void onProgressChanged(SeekBar seekBar, int progressValue, boolean fromUser) {
                progress = progressValue;
                textView2.setText("" + progress);

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }

        });

        this.seekBar3.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int progress = 0;


            @Override
            public void onProgressChanged(SeekBar seekBar, int progressValue, boolean fromUser) {
                progress = progressValue;
                textView3.setText("" + progress);

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }

        });

        this.seekBar4.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int progress = 0;


            @Override
            public void onProgressChanged(SeekBar seekBar, int progressValue, boolean fromUser) {
                progress = progressValue;
                textView4.setText("" + progress);

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }

        });

        this.seekBar5.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int progress = 0;


            @Override
            public void onProgressChanged(SeekBar seekBar, int progressValue, boolean fromUser) {
                progress = progressValue;
                textView5.setText("" + progress);

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }

        });

        this.seekBar6.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int progress = 0;


            @Override
            public void onProgressChanged(SeekBar seekBar, int progressValue, boolean fromUser) {
                progress = progressValue;
                textView6.setText("" + progress);

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }

        });


        /*
         * Listeners des titres des 6 critères de notation permettant d'ouvrir
         * une boite de dialogue lorsque le titre du critère est touché
         * La boite de dialogue contient une description du critère
         */
        this.originaliteView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder dialog = new AlertDialog.Builder(NotationActivity.this);
                dialog.setTitle("Originalité");
                dialog.setMessage("Le projet est original et innovant. ''Vous seriez prêt à l'aquérir''");
                dialog.create();
                dialog.show();
            }
        });
        this.prototypeView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder dialog = new AlertDialog.Builder(NotationActivity.this);
                dialog.setTitle("Prototype");
                dialog.setMessage("Le prototype est fonctionnel, innovant et le travail réalisé est conséquent");
                dialog.create();
                dialog.show();
            }
        });
        this.demarcheView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder dialog = new AlertDialog.Builder(NotationActivity.this);
                dialog.setTitle("Démarche SI");
                dialog.setMessage("Le projet s'appuie sur des expérimentations, de la simulation théorique et numérique avec une comparaison entre le réel et le modèle et une optimisation");
                dialog.create();
                dialog.show();
            }
        });
        this.pluriView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder dialog = new AlertDialog.Builder(NotationActivity.this);
                dialog.setTitle("Pluridisciplinarité");
                dialog.setMessage("Le projet mobilise plusieurs disciplines (Maths, SI, Physique, ...) et plusieurs technologies (transfert d'énergie, traitement de l'information, mécanique, ...)");
                dialog.create();
                dialog.show();
            }
        });
        this.maitriseView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder dialog = new AlertDialog.Builder(NotationActivity.this);
                dialog.setTitle("Maîtrise");
                dialog.setMessage("Le développement théorique est conséquent et bien maîtrisé");
                dialog.create();
                dialog.show();
            }
        });
        this.developpementView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder dialog = new AlertDialog.Builder(NotationActivity.this);
                dialog.setTitle("Communication");
                dialog.setMessage("La présentation est claire, structurée, dynamique. Elle valorise le travail d'équipe. Les réponses aux questions sont pertinentes");
                dialog.create();
                dialog.show();
            }
        });

        /*
         * Listeners des icones "+" des 6 critères de notation permettant d'ouvrir
         * une boite de dialogue lorsque l'icone du critère est touchée
         * La boite de dialogue contient une description du critère
         */

        this.originaliteIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder dialog = new AlertDialog.Builder(NotationActivity.this);
                dialog.setTitle("Originalité");
                dialog.setMessage("Le projet est original et innovant. ''Vous seriez prêt à l'aquérir''");
                dialog.create();
                dialog.show();
            }
        });
        this.prototypeIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder dialog = new AlertDialog.Builder(NotationActivity.this);
                dialog.setTitle("Prototype");
                dialog.setMessage("Le prototype est fonctionnel, innovant et le travail réalisé est conséquent");
                dialog.create();
                dialog.show();
            }
        });
        this.demarcheIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder dialog = new AlertDialog.Builder(NotationActivity.this);
                dialog.setTitle("Démarche SI");
                dialog.setMessage("Le projet s'appuie sur des expérimentations, de la simulation théorique et numérique avec une comparaison entre le réel et le modèle et une optimisation");
                dialog.create();
                dialog.show();
            }
        });
        this.pluriIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder dialog = new AlertDialog.Builder(NotationActivity.this);
                dialog.setTitle("Pluridisciplinarité");
                dialog.setMessage("Le projet mobilise plusieurs disciplines (Maths, SI, Physique, ...) et plusieurs technologies (transfert d'énergie, traitement de l'information, mécanique, ...)");
                dialog.create();
                dialog.show();
            }
        });
        this.maitriseIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder dialog = new AlertDialog.Builder(NotationActivity.this);
                dialog.setTitle("Maîtrise");
                dialog.setMessage("Le développement théorique est conséquent et bien maîtrisé");
                dialog.create();
                dialog.show();
            }
        });
        this.developpementIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder dialog = new AlertDialog.Builder(NotationActivity.this);
                dialog.setTitle("Communication");
                dialog.setMessage("La présentation est claire, structurée, dynamique. Elle valorise le travail d'équipe. Les réponses aux questions sont pertinentes");
                dialog.create();
                dialog.show();
            }
        });

    }


    /**
     * Méthode qui s'active lorsqu'on met l'activité sur pause
     */
    @Override
    protected void onPause() {
        if (ScreenReceiver.wasScreenOn) {
            System.out.println("SCREEN TURNED OFF");
        } else {
            System.out.println("SCREEN DIDN'T CHANGED");
        }
        super.onPause();
    }


    /**
     * Méthode qui s'active lorsqu'on sort de la pause et qu'on reprend l'activité
     */
    @Override
    protected void onResume() {
        if (!ScreenReceiver.wasScreenOn) {
            System.out.println("SCREEN TURNED ON");
        }
        super.onResume();
    }


    /**
     * méthode renvoyant la progression de la barre de progression originalité
     * @return un entier correspondant à la note
     */
    public int getProgressbar1(){
        return this.seekBar1.getProgress();
    }


    /**
     * méthode renvoyant la progression de la barre de progression prototype
     * @return un entier correspondant à la note
     */
    public int getProgressbar2(){
        return this.seekBar2.getProgress();
    }


    /**
     * méthode renvoyant la progression de la barre de progression démarche SI
     * @return un entier correspondant à la note
     */
    public int getProgressbar3(){
        return this.seekBar3.getProgress();
    }


    /**
     * méthode renvoyant la progression de la barre de progression pluridisciplinarité
     * @return un entier correspondant à la note
     */
    public int getProgressbar4(){
        return this.seekBar4.getProgress();
    }


    /**
     * méthode renvoyant la progression de la barre de progression maîtrise
     * @return un entier correspondant à la note
     */
    public int getProgressbar5(){
        return this.seekBar5.getProgress();
    }


    /**
     * méthode renvoyant la progression de la barre de progression communication
     * @return un entier correspondant à la note
     */
    public int getProgressbar6(){
        return this.seekBar6.getProgress();
    }
}