package com.a2a1.olympiadesi;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Classe qui s'occupe de gérer l'état de l'écran
 * @author Antoine Tamin
 * @see BroadcastReceiver
 */
public class ScreenReceiver extends BroadcastReceiver {

    public static boolean wasScreenOn = true;


    /**
     * Change le boolean suivant l'état de l'écran
     * @param context contexte depuis lequel cette classe est appellée
     * @param intent Intent depuis lequel on appelle cette classe
     */
    @Override
    public void onReceive(Context context, Intent intent) {
        try {

            /*
             * Si l'écran s'éteint on passe le boolean à false
             */
            if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {
                context.stopService(intent);
                wasScreenOn = false;
            }
        }
        catch (NullPointerException e) {
            System.out.println(e);
        }
    }
}
