package com.a2a1.olympiadesi;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.JDBC.Notes;
import com.JSON.JSONSystem;

import org.json.JSONException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

/**
 * Classe qui correspond à l'activité du planning
 * @author Antoine Tamin
 * @see AppCompatActivity
 */
public class ScheduleActivity extends AppCompatActivity {

    private LinearLayout linearLayoutMain;
    private TextView numberJury;
    private Button decoButton, sendResultButton;

    private IntentFilter filter;
    private BroadcastReceiver mReceiver;

    private int REQUEST_CODE;
    private int numberGroups;
    private int index;

    private ArrayList<HashMap<Object, Object>> listeGroups;
    private ArrayList<RowElement> rowElementList;
    private ArrayList<Notes> notesList;
    private ArrayList<Notes> purgeList;
    private ArrayList<RowElement> rowElementLateList;

    private HashMap<String, Integer> dicoIndex;


    /**
     Crée la vue et assigne les listeners aux boutons.
     @param savedInstanceState ensemble des informations concernant l'activité
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schedule);

        numberGroups = 0;
        REQUEST_CODE = 1;
        listeGroups = (ArrayList<HashMap<Object, Object>>) JSONSystem.getDataByKey("groupes");
        rowElementList = new ArrayList<>();
        purgeList = new ArrayList<>();
        notesList = new ArrayList<>();
        rowElementLateList = new ArrayList<>();
        dicoIndex = new HashMap<>();


        for (HashMap<Object, Object> hashmap : listeGroups) {
            numberGroups += 1;
        }

        /*
         * Récupération de l'intent parent
         */

        Intent intentParent = getIntent();

        /*
         * Filtre qui vérifie si la tablette s'éteint afin de déconnecter l'utilisateur
         */

        filter = new IntentFilter(Intent.ACTION_SCREEN_ON);
        filter.addAction(Intent.ACTION_SCREEN_OFF);
        mReceiver = new ScreenReceiver();
        registerReceiver(mReceiver, filter);

        /*
         * Branchement des différents éléments graphiques
         */

        numberJury = findViewById(R.id.scheduleActivityInputJury);
        decoButton = findViewById(R.id.scheduleActivityDecoButton);
        sendResultButton = findViewById(R.id.scheduleActivitySendResultButton);
        linearLayoutMain = findViewById(R.id.scheduleActivityLayoutContainer);

        /*
         * Récupération de l'idJury via l'intent parent et attribution de la valeur au TextView
         */

        final String idJury = intentParent.getStringExtra("id");
        numberJury.setText(" " + idJury);

        /*
         * Thread qui vérifie la connexion en permanence afin d'autoriser ou non l'envoie des
         * données
         */

        final Thread manager = new Thread() {
            @Override
            public void run() {
                try {

                    /*
                     * On effectue la vérification tant que le thread n'est pas arrêté
                     */

                    while (!isInterrupted()) {
                        ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                        final NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
                        final WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
                        final WifiInfo connectionInfo = wifiManager.getConnectionInfo();
                        final String ssid = connectionInfo.getSSID();

                        try {
                            Thread.sleep(10);
                        }
                        catch (InterruptedException e) {
                            System.out.println(e);
                        }

                        /*
                         * Modifications visuelles sur la vue suivant l'état du wifi
                         */

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                /*
                                 * On colorie le bouton en vert si on est connecté au bon wifi
                                 */

                                if (mWifi.isConnected() && ssid.equals("\"" + getString(R.string.nomReseau) + "\"")) {
                                    sendResultButton.setClickable(true);
                                    sendResultButton.setBackgroundColor(Color.parseColor("#4caf50"));
                                }

                                /*
                                 * Sinon on grise le bouton et il n'est plus cliquable
                                 */

                                else {
                                    sendResultButton.setClickable(false);
                                    sendResultButton.setBackgroundColor(Color.GRAY);
                                }
                            }
                        });
                    }
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        manager.start();

        /*
         * Thread qui vérifie en permanence si un l'horaire de notation d'un groupe est dépassé
         */

        final Thread managerLate = new Thread() {

            @Override
            public void run() {
                try {

                    /*
                     * Tant que le thread n'est pas arrêté on vérifie l'horaire de fin de chaque
                     * groupe
                     */

                    while (!isInterrupted()) {
                        Thread.sleep(100);

                        Calendar calendar = Calendar.getInstance();
                        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");

                        for (final RowElement rowElement : rowElementList) {

                            String currentDate = sdf.format(calendar.getTime());
                            String timeString = rowElement.getEndHours();

                            /*
                             * On formate les strings de l'heure actuelle et de l'heure récupérée
                             * suivant le format défini
                             */
                            Date currentHour = sdf.parse(currentDate);
                            Date timeHour = sdf.parse(timeString);

                            /*
                             * Si l'heure de fin de notation du groupe est dépassé et qu'on a pas
                             * déjà averti le juré ou noté ce groupe on affiche un message
                             * d'avertissement
                             */

                            if(currentHour.compareTo(timeHour) > 0 && !rowElementLateList.contains(rowElement)) {
                                rowElementLateList.add(rowElement);

                                /*
                                 * Méthode qui effectue les modifications sur la vue en affichant
                                 * le message d'alerte
                                 */
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        AlertDialog.Builder alert = new AlertDialog.Builder(ScheduleActivity.this);
                                        alert.setTitle("Retard");
                                        alert.setMessage("Attention vous êtes en retard, le groupe " +
                                                rowElement.getNameGroup() + " aurait déjà dû être noté");
                                        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                dialogInterface.dismiss();
                                            }
                                        });
                                        alert.create();
                                        alert.show();
                                    }
                                });
                            }
                            //System.out.println("Current Hour " + currentHour + " group hour " + timeHour);

                            try {
                                Thread.sleep(100);
                            }
                            catch (InterruptedException e) {
                                System.out.println(e);
                            }
                        }
                    }
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };

        /*
         * Listener du bouton de déconnexion qui affiche un message pour demander confirmation
         */
        decoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alert = new AlertDialog.Builder(ScheduleActivity.this);
                alert.setTitle("Deconnexion");
                alert.setMessage("Etes-vous sûr de vouloir vous déconnecter ?");
                alert.setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        /*
                         * On arrête les threads en cours et on coupe l'activité en cours
                         */

                        manager.interrupt();
                        managerLate.interrupt();
                        unregisterReceiver(mReceiver);
                        finish();
                    }
                });
                alert.setNegativeButton("Non", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                alert.create();
                alert.show();
            }
        });

        /*
         * Listener qui démarre la tâche asynchrone d'envoie des notes au JSON
         */

        sendResultButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (notesList.isEmpty()) {
                    Toast.makeText(getApplicationContext(), "Aucunes données à envoyer", Toast.LENGTH_SHORT).show();
                }
                else {
                    EnvoieAsyncTask envoieAsyncTask = new EnvoieAsyncTask();
                    envoieAsyncTask.execute(notesList);
                }
            }
        });

        /*
         * Création de bouton de groupe pour chaque groupe récupéré dans le fichier JSON
         */
        for (int i = 0; i < numberGroups; i++) {

            this.index = i;

            final RowElement rowElement = new RowElement(this);
            rowElement.createRowElement(i);

            dicoIndex.put(rowElement.getNameGroup(), i);
            rowElementList.add(rowElement);

            /*
             * Ajout du bouton de groupe au layout principal de la vue
             */

            linearLayoutMain.addView(rowElement.getLinearLayout());

            /*
             * Listener du bouton de groupe qui démarre l'activité de notation du groupe et envoie
             * les informations du groupe à travers l'Intent
             */
            rowElement.getButtonGroup().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent notationView = new Intent(getApplicationContext(), NotationActivity.class);
                    notationView.putExtra("indexGroup", getIdByName(rowElement.getNameGroup()));
                    notationView.putExtra("idJury", Integer.parseInt(idJury));
                    notationView.putExtra("numéro", rowElement.getIdProjet());
                    System.out.println("idp " + rowElement.getIdProjet());
                    startActivityForResult(notationView, REQUEST_CODE);
                }
            });
        }
        managerLate.start();

    }


    /**
     * Méthode qui s'active lorsqu'on met l'activité sur pause
     */
    @Override
    protected void onPause() {
        if (ScreenReceiver.wasScreenOn) {
            System.out.println("SCREEN OFF");
        } else {
            System.out.println("PAS DE CHANGEMENT");
        }
        super.onPause();
    }


    /**
     * Méthode qui s'active lorsqu'on sort de la pause et qu'on reprend l'activité
     */
    @Override
    protected void onResume() {
        if (!ScreenReceiver.wasScreenOn) {
            System.out.println("SCREEN ON");
        }
        super.onResume();
    }


    /**
     * Destruction de l'activité et démarrage de la tâche asynchrone qui envoie les données au JSON
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (!notesList.isEmpty()) {
            EnvoieAsyncTask envoieAsyncTask = new EnvoieAsyncTask();
            envoieAsyncTask.execute(notesList);
        }
    }


    /**
     * Renvoie l'id du groupe
     * @param name le nom du groupe
     * @return l'id du groupe correspondant au nom du groupe
     */
    public int getIdByName(String name) {
        return dicoIndex.get(name);
    }


    /**
     * Effectue les modifications après la fin de l'intent lancé précédemment et la récupération du
     * résultat renvoyé par l'intent
     * @param requestCode code que l'on s'attend à avoir
     * @param resultCode code que l'on reçoit
     * @param data Intent qui a renvoyé le résultat
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK && data.getStringExtra("etat").equals("valide")) {
                System.out.println("VALIDE");

                /*
                 * Récupération des données renvoyées par l'intent
                 */

                int idP = data.getIntExtra("numéro", 0);
                int idJury = data.getIntExtra("idJury", 0);
                String nameGroup = data.getStringExtra("nameGroup");
                int originalite = data.getIntExtra("originalite", 0);
                int prototype = data.getIntExtra("prototype", 0);
                int demarcheSI = data.getIntExtra("demarcheSI", 0);
                int pluridisciplinarite = data.getIntExtra("pluridisciplinarite", 0);
                int maitrise = data.getIntExtra("maitrise", 0);
                int developpement = data.getIntExtra("developpement", 0);
                String etat = data.getStringExtra("etat");

                /*System.out.println("ipP " + idP);
                System.out.println("idJury " + idJury);
                System.out.println("nameGroup " + nameGroup);
                System.out.println("originalite " + originalite);
                System.out.println("prototype " + prototype);
                System.out.println("demarcheSI " + demarcheSI);
                System.out.println("pluridisciplinarite " + pluridisciplinarite);
                System.out.println("maitrise " + maitrise);
                System.out.println("developpement " + developpement);
                System.out.println("etat " + etat);*/

                /*
                 * On change la valeur de l'état du groupe par la valeur renvoyée par l'Intent
                 * et on change la couleur du bouton d'état en vert puisqu'il est noté
                 */

                JSONSystem.setValueInGroupe("etat", this.index, "valide");

                for (RowElement rowElement : rowElementList) {
                    if (rowElement.getNameGroup().equals(nameGroup)) {
                        rowElement.getButtonState().setBackgroundColor(Color.parseColor("#4caf50"));
                        rowElement.setState("valide");
                    }
                }

                /*
                 * On crée un objet Note et on l'ajoute à la liste qui sera envoyée au fichier
                 * JSON plus tard
                 */

                Notes notes = new Notes(idJury, "" + idP, originalite, prototype, demarcheSI, pluridisciplinarite, maitrise, developpement, etat);
                notesList.add(notes);

                Toast.makeText(getApplicationContext(), "Le groupe " + nameGroup + " a été noté", Toast.LENGTH_SHORT).show();

            }

            else if (resultCode == Activity.RESULT_OK && data.getStringExtra("etat").equals("absent")) {

                /*
                 * Récupération des données renvoyées par l'intent
                 */

                System.out.println("ABSENT");
                int idP = data.getIntExtra("numéro", 0);
                int idJury = data.getIntExtra("idJury", 0);
                String nameGroup = data.getStringExtra("nameGroup");
                int originalite = data.getIntExtra("originalite", 0);
                int prototype = data.getIntExtra("prototype", 0);
                int demarcheSI = data.getIntExtra("demarcheSI", 0);
                int pluridisciplinarite = data.getIntExtra("pluridisciplinarite", 0);
                int maitrise = data.getIntExtra("maitrise", 0);
                int developpement = data.getIntExtra("developpement", 0);
                String etat = data.getStringExtra("etat");

                /*System.out.println("ipP " + idP);
                System.out.println("idJury " + idJury);
                System.out.println("nameGroup " + nameGroup);
                System.out.println("originalite " + originalite);
                System.out.println("prototype " + prototype);
                System.out.println("demarcheSI " + demarcheSI);
                System.out.println("pluridisciplinarite " + pluridisciplinarite);
                System.out.println("maitrise " + maitrise);
                System.out.println("developpement " + developpement);
                System.out.println("etat " + etat);*/

                /*
                 * On change la valeur de l'état du groupe par la valeur renvoyée par l'Intent
                 * et on change la couleur du bouton d'état en rouge puisqu'il est absent
                 */

                JSONSystem.setValueInGroupe("etat", this.index, "absent");

                for (RowElement rowElement : rowElementList) {
                    if (rowElement.getNameGroup().equals(nameGroup)) {
                        rowElement.getButtonState().setBackgroundColor(Color.parseColor("#c0341d"));
                        rowElement.setState("absent");
                    }
                }

                /*
                 * On crée un objet Note et on l'ajoute à la liste qui sera envoyée au fichier
                 * JSON plus tard
                 */

                Notes notes = new Notes(idJury, "" + idP, originalite, prototype, demarcheSI, pluridisciplinarite, maitrise, developpement, etat);
                notesList.add(notes);

                Toast.makeText(getApplicationContext(), "Le groupe " + nameGroup + " a été noté absent", Toast.LENGTH_SHORT).show();


            } else {
                System.out.println("Modifications annulées");
            }
        }
    }


    /**
     * Arrêt de l'activité lorsqu'on appuie sur le bouton retour de la tablette
     */
    @Override
    public void onBackPressed() {
        unregisterReceiver(mReceiver);
        finish();
    }


    /**
     * Tâche asynchrone qui envoie les notes au fichier JSON
     * @see AsyncTask
     */
    private class EnvoieAsyncTask extends AsyncTask<ArrayList, Void, String> {

        private boolean flag = false;


        /**
         * Appelle la méthode d'envoie des données au fichier JSON
         * @param arrayLists la liste des notes de chaque groupe
         * @return un string correspondant à ce que l'on veut renvoyer comme résultat
         */
        @Override
        protected String doInBackground(ArrayList[] arrayLists) {
            envoieData();
            return "true";
        }


        /**
         * Envoie chaque note de la liste des notes au JSON qui fait une mise à jour
         */
        public void envoieData() {
            for (Notes notes : notesList) {
                try {
                    purgeList.add(notes);
                    JSONSystem.updateInBD(notes.getIdJ(), notes.getIdP(), notes);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            purgeListeNotes();
            flag = true;
        }


        /**
         * Suppression des notes de la liste afin d'éviter de renvoyer des choses non modifiées
         */
        public void purgeListeNotes() {
            notesList.removeAll(purgeList);
        }


        /**
         * Affiche un message de confirmation une fois les notes envoyées au fichier JSON
         * @param s le string renvoyé par doInBackground()
         */
        @Override
        protected void onPostExecute(String s) {
            if (flag) {
                Toast.makeText(getApplicationContext(), "Données envoyées", Toast.LENGTH_SHORT).show();

            }
        }
    }
}
