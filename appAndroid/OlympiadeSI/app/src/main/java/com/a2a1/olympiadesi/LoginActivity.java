package com.a2a1.olympiadesi;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.JSON.BadIDException;
import com.JSON.JSONSystem;

/**
 * Classe qui correspond à l'activité de connexion
 * @author Antoine Tamin
 * @see AppCompatActivity
 */
public class LoginActivity extends AppCompatActivity {


    private EditText loginInput;
    private EditText passwordInput;
    private Button connectionButton;

    private Activity activity;


    /**
     Crée la vue et assigne les listeners aux boutons.
     @param savedInstanceState ensemble des informations concernant l'activité
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        this.activity = this;

        //Branchement des différents éléments graphiques

        loginInput = findViewById(R.id.loginActivityLoginInput);
        passwordInput = findViewById(R.id.loginActivityPasswordInput);
        connectionButton = findViewById(R.id.loginActivityButtonConnection);

        //Listener du bouton de connexion qui lance la tâche asynchrone

        connectionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MyAsyncTask myAsyncTask = new MyAsyncTask(activity);
                myAsyncTask.execute(loginInput.getText().toString(), passwordInput.getText().toString());
            }
        });
    }


    /**
     Obtenir un EditText.
     @return l'EditText qui contient le login saisi
     */
    public EditText getLoginInput() {
        return loginInput;
    }


    /**
     Obtenir un EditText.
     @return l'EditText qui contient le mot de passe saisi
     */
    public EditText getPasswordInput() {
        return passwordInput;
    }


    /**
     * Tâche asynchrone qui prépare le fichier JSON et effectue les vérifications de connexion
     * @author Antoine Tamin
     * @see AsyncTask
     */
    private class MyAsyncTask extends AsyncTask<String, Void, String> {

        private String login;
        private String password;
        private String loginInput;
        private String passwordInput;

        private int id ;

        private Activity activity;


        /**
         * Constructeur de la tâche asynchrone
         * @param activity activité dans laquelle la tâche est lancée
         */
        public MyAsyncTask(Activity activity) {
            this.activity = activity;
        }


        /**
         * Effectue les vérifications sur le wifi et les identifants saisis en arrière plan
         * @param strings les éléments envoyés lors du lancement de la tâche asynchrone
         * @return un string correspondant à ce que l'on veut renvoyer
         */
        @Override
        protected String doInBackground(String... strings) {

            /*
             * Gestion de la wifi, récupération du réseau wifi, récupération du SSID (nom)
             */

            ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            final NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            final WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
            final WifiInfo connectionInfo = wifiManager.getConnectionInfo();
            String ssid = connectionInfo.getSSID();

            /*
             * Vérification de la connexion au bon réseau wifi en comparant le SSID au nom enregistré
             * dans le fichier res/strings
             */

            if (mWifi.isConnected() && ssid.equals("\"" + getString(R.string.nomReseau) + "\"")) {

                /*
                 * On vérifie que les champs de connexion ne soient pas vide
                 */
                if (!strings[0].isEmpty() && !strings[1].isEmpty()) {
                    loginInput = strings[0];
                    passwordInput = strings[1];

                    /*
                     * On prépare le fichier JSON si il n'existe pas déjà (première connexion)
                     */
                    if (JSONSystem.json.size() == 0) {
                        try {
                            JSONSystem.prepareJSONFile(strings[0], strings[1]);
                        } catch (BadIDException e) {
                            return "BAD IDs";
                        }
                    }

                    login = (String) JSONSystem.getDataByKey("login");
                    password = (String) JSONSystem.getDataByKey("pass");
                    id = (int) JSONSystem.getDataByKey("idJury");

                    /*
                     * On vérifie que les champs saisis correspondent bien aux champs récupérés
                     * dans le fichier JSON lors de la préparation du fichier
                     */
                    if (loginInput.equals(login) && passwordInput.equals(password)) {
                        Intent scheduleActivity = new Intent(activity, ScheduleActivity.class);
                        scheduleActivity.putExtra("id", String.valueOf(id));
                        activity.startActivity(scheduleActivity);
                        return "OK";
                    }
                }
                else {
                    return "NO IDs";
                }
            }
            else {
                System.out.println("Erreur de réseau");
            }
            return "NOT OK";
        }


        /**
         * Effectue les actions visibles sur l'écran après la fonction doInBackground()
         * @param string renvoyé par la méthode doInBackground()
         */
        @Override
        protected void onPostExecute(String s) {
            switch (s) {
                case "BAD IDs" :

                    /*
                     * Si les idenfitifants sont incorrectes on fait vibrer la vue, on affiche un
                     * message d'erreur
                     */

                    View viewContent = activity.findViewById(R.id.loginActivityContent);
                    Animation shake = AnimationUtils.loadAnimation(activity, R.anim.shake);
                    viewContent.startAnimation(shake);
                    Toast.makeText(activity.getApplicationContext(), "Identifiants incorrectes", Toast.LENGTH_LONG).show();
                    System.out.println(login);
                    System.out.println(password);
                    System.out.println(id);
                    break;

                case "OK" :

                    /*
                     * Si les idenfiants sont corrects ont reset les champs
                     */

                    getLoginInput().setText(null);
                    getPasswordInput().setText(null);
                    break;

                case "NO IDs" :

                    /*
                     * Si aucun identifiant n'est saisi on affiche un message d'erreur
                     */

                    Toast.makeText(getApplicationContext(), "Veuillez saisir vos identifiants", Toast.LENGTH_SHORT).show();
                    break;

                case "NOT OK" :

                    /*
                     * Si on est pas connecté au bon réseau wifi au moment de la connexion
                     * on affiche un message d'erreur
                     */

                    Toast.makeText(getApplicationContext(), "Vous n'êtes pas connecté au bon réseau wifi", Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    }
}
