package com.JSON;

import com.JDBC.*;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.*;

public class JSONSystem {

    public static HashMap<Object,Object> json = new HashMap<>();
    private static SimpleDateFormat format = new SimpleDateFormat("k:mm");

    /**
     *
     * @param key
     * @return
     */
    public static Object getDataByKey(String key){
        Object value = json.get(key);
        return value;
    }

    /**
     *
     * @param key
     * @param index
     * @return
     */
    public static Object getValueInGroupes(String key, int index){
        ArrayList<HashMap<Object,Object>> groupes = (ArrayList<HashMap<Object, Object>>) json.get("groupes");
        return groupes.get(index).get(key);
    }

    /**
     *
     * @param key
     * @param index
     * @param value
     */
    public static void setValueInGroupe(String key, int index, Object value){
        ArrayList<HashMap<Object,Object>> groupes = (ArrayList<HashMap<Object, Object>>) json.get("groupes");
        groupes.get(index).put(key,value);
    }

    /**
     *
     * @param key
     * @param value
     */
    public static void setValue(String key, Object value){
        json.put(key,value);
    }


    /**
     *
     * @param login
     * @param mdp
     * @return
     * @throws BadIDException
     */
    public static boolean prepareJSONFile(String login,String mdp) throws BadIDException{
        json = null;
        json = new HashMap<>();
        ArrayList<InfoProjet> infoProjets = new ArrayList<>();


        Jury jury = null;
        try {
            System.out.println("Mdp = " + mdp);
            jury = requeteJury(login,mdp);
        }
            catch (JSONException e){
            throw new BadIDException();
        }
        try {
            Sponsor sponsor = null;
            sponsor = requeteSponsor();
            infoProjets = requeteInfoProjets(jury.getIdJ());
            Collections.sort(infoProjets);
            json.put("idJury", jury.getIdJ());
            json.put("login", login);
            json.put("pass", mdp);
            json.put("sponsor_image", sponsor.getImage());

            InfoProjet projet_actual = null;

            ArrayList<HashMap<Object,Object>> groupes  = new ArrayList<>();

            for (int i = 0; i < infoProjets.size()  ; i++) {
                projet_actual = infoProjets.get(i);
                HashMap<Object,Object> groupe_values = new HashMap<>();
                groupe_values.put("nom",projet_actual.proj.getNomPro());
                groupe_values.put("numéro",projet_actual.proj.getIdP());
                groupe_values.put("description",projet_actual.proj.getDescription());
                groupe_values.put("image",projet_actual.proj.getImage());
                groupe_values.put("emplacement",projet_actual.proj.getEmplacement());
                groupe_values.put("horaireDeb",projet_actual.cren.hDebut);
                groupe_values.put("horaireFin",projet_actual.cren.hFin);
                groupe_values.put("etat",projet_actual.not.getEtat());
                groupe_values.put("originalite",projet_actual.not.getOriginalite());
                groupe_values.put("prototype",projet_actual.not.getPrototype());
                groupe_values.put("demarcheSI",projet_actual.not.getDemarche_SI());
                groupe_values.put("pluridisciplinarite",projet_actual.not.getPluridisciplinarite());
                groupe_values.put("maitrise",projet_actual.not.getMaitrise());
                groupe_values.put("developpement_durable",projet_actual.not.getDvptDurable());

                groupes.add(groupe_values);
            }
            json.put("groupes",groupes);
            System.out.println("Json ="+json);

            return true;
        }
        catch (Exception e) {
            e.printStackTrace();
            System.out.println(e);
            return false;
        }
    }


    /**
     *
     * @param idj
     * @param idp
     * @param notes
     * @return
     * @throws JSONException
     */
    public static int updateInBD(String idj,String idp,Notes notes) throws JSONException{
        Map<String,String> params = new HashMap<>();
        params.put("idJ",idj);
        params.put("idP",idp);

        params.put("originalite",""+notes.originalite);
        params.put("prototype",""+notes.prototype);
        params.put("demarche_Scientifique",""+notes.demarche_Scientifique);
        params.put("pluridisciplinarite",""+notes.pluridisciplinarite);
        params.put("Maitrise_Scientifique",""+notes.Maitrise_Scientifique);
        params.put("Communication",""+notes.Communication);
        params.put("Etat",""+notes.etat);


        JSONObject jsonObject = null;
        String DATA = null;
        try {
            jsonObject = convertJSONString2Obj("existsInBD",getJSONStringWithParam_POST(Donnes_BD.SEND_TO_BD,
                    params));
            DATA =(String)jsonObject.get("existsInBD");
            System.out.println(DATA);
        }catch (IOException exception){exception.printStackTrace();}
        JSONObject parsed = new JSONObject(DATA);
        return parsed.getInt("existsInBD");
    }

    /**
     *
     * @param params
     * @return
     * @throws JSONException
     * @throws BadIDException
     */
    private static Jury requeteJury(String... params) throws JSONException, BadIDException {
        Map<String,String> param = new HashMap<>();
        JSONObject parsed = null;
        param.put("login", params[0]);
        param.put("mdp", params[1]);
        System.out.println("Param : " + param);
        Jury jury = null;
        JSONObject jsonObject = null;
        String DATA=null;
        try {
           jsonObject = convertJSONString2Obj("existsInBD",getJSONStringWithParam_POST(Donnes_BD.LOGIN_URL,
                   param));
           DATA = (String)jsonObject.get("existsInBD");
           System.out.println("data = " + DATA);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        parsed = new JSONObject(DATA);
        jury = new Jury(parsed.getInt("idJ"),param.get("login"),param.get("mdp"));

        return  jury;
    }

    /**
     *
     * @return
     * @throws JSONException
     */
    private static Sponsor requeteSponsor() throws JSONException{
        JSONObject jsonObject = null;
        Sponsor sponsor = null;
        JSONObject parsed = null;
        String DATA = null;
        try {
            jsonObject = convertJSONString2Obj("sponsor",getJSONStringWithParam_POST(Donnes_BD.SPONSOR,""));
            DATA = (String)jsonObject.get("sponsor");
        }catch (IOException e){
            e.printStackTrace();
        }
        parsed = new JSONObject(DATA);
        sponsor = new Sponsor(parsed.getString("nom"),parsed.getString("type"),parsed.getString("image"));
        return sponsor;
    }

    /**
     *
     * @param idJ
     * @return
     * @throws JSONException
     */
    private static ArrayList<InfoProjet> requeteInfoProjets(int idJ) throws JSONException {
        Map<String,String> param = new HashMap<>();
        param.put("idJ", ""+idJ);

        ArrayList<InfoProjet> infoProjets = new ArrayList<>();
        JSONObject jsonObject = null;
        JSONObject parsed = null;
        String Data = null;
        try {
            jsonObject = convertJSONString2Obj("infoProjet",getJSONStringWithParam_POST(Donnes_BD.INFO_PROJETS,param));
            Data = jsonObject.getString("infoProjet");
            System.out.println("Data = "+ Data);

        }catch (IOException e){
            e.printStackTrace();
        }
        parsed = new JSONObject(Data);
        JSONArray creneau = parsed.getJSONArray("creneau");
        JSONArray notes  = parsed.getJSONArray("notes");
        JSONArray projet = parsed.getJSONArray("projet");

        JSONObject actualCreneau;
        JSONObject actualNotes;
        JSONObject actualProjet;

        for (int i = 0; i < creneau.length(); i++) {
            actualCreneau = ((JSONObject) creneau.get(i));
            actualNotes = ((JSONObject) notes.get(i));
            actualProjet = ((JSONObject) projet.get(i));
            infoProjets.add(
                    new InfoProjet(
                            new Notes(
                                    actualNotes.getInt("idJ"),
                                    actualNotes.getString("idP"),
                                    actualNotes.getInt("originalite"),
                                    actualNotes.getInt("prototype"),
                                    actualNotes.getInt("Demarche_Scientifique"),
                                    actualNotes.getInt("pluridisciplinarite"),
                                    actualNotes.getInt("Maitrise_Scientifique"),
                                    actualNotes.getInt("Communication"),
                                    actualNotes.getString("etat")
                            ),
                            new Projet(
                                    actualProjet.getString("idP"),
                                    actualProjet.getString("nomPro"),
                                    actualProjet.getString("description"),
                                    actualProjet.getString("image"),
                                    actualProjet.getString("emplacement")
                            ),
                            new Creneau(
                                    actualCreneau.getInt("idC"),
                                    (actualCreneau.getString("hdebut")),
                                    (actualCreneau.getString("hfin"))
                            )

                    )
            );
        }



        return infoProjets;
    }


    /**
     *
     * @param url
     * @return
     */
    public static String getJSONStringFromUrl_GET(String url) {
        JSONArray jsonArray = null;
        HttpURLConnection httpURLConnection = null;
        BufferedReader bufferedReader;
        StringBuilder stringBuilder;
        String line;
        String jsonString = "";
        try {
            URL u = new URL(url);
            httpURLConnection = (HttpURLConnection) u.openConnection();
            httpURLConnection.setRequestMethod("GET");
            bufferedReader = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
            stringBuilder = new StringBuilder();

            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line + '\n');
            }
            jsonString = stringBuilder.toString();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            httpURLConnection.disconnect();
        }

        return jsonString;
    }




    /**
     * Convert json string to json object
     * @param jsonString
     * @return
     */
    public static JSONObject convertJSONString2Obj(String key,String jsonString) {
        JSONObject jObj = null;

        jObj = new JSONObject();
        try {
            jObj.put(key,jsonString);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jObj;
    }

    /**
     * Get json string from URL with method POST
     * @param serviceUrl
     * @param params post data
     * @return json string
     */
    public static String getJSONStringWithParam_POST(String serviceUrl, Map<String, String> params)
    throws IOException
    {
        JSONArray jsonArray = null;
        String jsonString = null;
        HttpURLConnection conn = null;
        String line;

        URL url;
        try
        {
            url = new URL(serviceUrl);
        }
        catch (MalformedURLException e)
        {
            throw new IllegalArgumentException("invalid url: " + serviceUrl);
        }

        StringBuilder bodyBuilder = new StringBuilder();
        Iterator<Map.Entry<String, String>> iterator = params.entrySet().iterator();
        // constructs the POST body using the parameters
        while (iterator.hasNext())
        {
            Map.Entry<String, String> param = iterator.next();
            bodyBuilder.append(param.getKey()).append('=')
                    .append(param.getValue());
            if (iterator.hasNext()) {
                bodyBuilder.append('&');
            }
        }

        String body = bodyBuilder.toString();
        byte[] bytes = body.getBytes();
        try {

            conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setUseCaches(false);
            conn.setFixedLengthStreamingMode(bytes.length);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type","application/x-www-form-urlencoded;charset=UTF-8");
            // post the request
            OutputStream out = conn.getOutputStream();
            out.write(bytes);
            out.close();
            // handle the response
            int status = conn.getResponseCode();

            if (status != 200) {
                throw new IOException("Post failed with error code " + status);
            }

            BufferedReader  bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            StringBuilder stringBuilder = new StringBuilder();


            while ((line = bufferedReader.readLine()) != null)
            {
                stringBuilder.append(line + '\n');
            }

            jsonString = stringBuilder.toString();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            conn.disconnect();
        }

        return jsonString;
    }


    /**
     *
     * @param serviceUrl
     * @param params
     * @return
     * @throws IOException
     */
    public static String getJSONStringWithParam_POST(String serviceUrl,String params)
    throws IOException
    {
        JSONArray jsonArray = null;
        String jsonString = null;
        HttpURLConnection conn = null;
        String line;

        URL url;
        try
        {
            url = new URL(serviceUrl);
        }
        catch (MalformedURLException e)
        {
            throw new IllegalArgumentException("invalid url: " + serviceUrl);
        }

        byte[] bytes = params.getBytes();
        try {

            conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setUseCaches(false);
            conn.setFixedLengthStreamingMode(bytes.length);
            conn.setRequestMethod("POST");
            //conn.setRequestProperty("Content-Type","application/x-www-form-urlencoded;charset=UTF-8");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.connect();
            // post the request
            OutputStream out = conn.getOutputStream();
            out.write (bytes);
            out.close();
            // handle the response
            int status = conn.getResponseCode();
            if (status != 200) {
                throw new IOException("Post failed with error code " + status);
            }

            BufferedReader  bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            StringBuilder stringBuilder = new StringBuilder();


            while ((line = bufferedReader.readLine()) != null)
            {
                stringBuilder.append(line + '\n');
            }

            jsonString = stringBuilder.toString();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            conn.disconnect();
        }

        return jsonString;
    }

    /**
     * Class privé contenant les liens http vers les fichiers pouvant réaliser les requêtes
     */
    private static final class Donnes_BD{

        public static String LOGIN_URL = "http://192.168.1.101/projet/siteWeb/traitement/traitementAndroid/traitementLogin.php";
        public static String SPONSOR = "http://192.168.1.101/projet/siteWeb/traitement/traitementAndroid/traitementSponsor.php";
        public static String INFO_PROJETS = "http://192.168.1.101/projet/siteWeb/traitement/traitementAndroid/traitementInfoProjet.php";
        public static String SEND_TO_BD = "http://192.168.1.101/projet/siteWeb/traitement/traitementAndroid/sendToBD.php";
    }
}
