package com.JDBC;

import java.io.Serializable;

public class Notes implements Serializable {
    public int idJ;
    public String idP;
    public int originalite;
    public int prototype;
    public int demarche_Scientifique;
    public int pluridisciplinarite;
    public int Maitrise_Scientifique;
    public int Communication;
    public String etat;

    public Notes(int idJ, String idP, int originalite, int prototype, int demarche_SI, int pluridisciplinarite, int maitrise, int dvptDurable,String etat) {
        this.idJ = idJ;
        this.idP = idP;
        this.originalite = originalite;
        this.prototype = prototype;
        this.demarche_Scientifique = demarche_SI;
        this.pluridisciplinarite = pluridisciplinarite;
        this.Maitrise_Scientifique = maitrise;
        this.Communication = dvptDurable;
        this.etat =etat;
    }

    public String getIdJ() {
        return String.valueOf(idJ);
    }

    public void setIdJ(int idJ) {
        this.idJ = idJ;
    }

    public String getIdP() {
        return idP;
    }

    public void setIdP(String idP) {
        this.idP = idP;
    }

    public int getOriginalite() {
        return originalite;
    }

    public void setOriginalite(int originalite) {
        this.originalite = originalite;
    }

    public int getPrototype() {
        return prototype;
    }

    public void setPrototype(int prototype) {
        this.prototype = prototype;
    }

    public int getDemarche_SI() {
        return demarche_Scientifique;
    }

    public void setDemarche_SI(int demarche_SI) {
        this.demarche_Scientifique = demarche_SI;
    }

    public int getPluridisciplinarite() {
        return pluridisciplinarite;
    }

    public void setPluridisciplinarite(int pluridisciplinarite) {
        this.pluridisciplinarite = pluridisciplinarite;
    }

    public int getMaitrise() {
        return Maitrise_Scientifique;
    }

    public void setMaitrise(int maitrise) {
        this.Maitrise_Scientifique = maitrise;
    }

    public int getDvptDurable() {
        return Communication;
    }

    public void setDvptDurable(int dvptDurable) {
        this.Communication = dvptDurable;
    }

    public String getEtat() {
        return etat;
    }
}
