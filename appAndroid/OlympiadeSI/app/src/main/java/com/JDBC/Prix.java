package com.JDBC;

public class Prix {
    public int getIdP() {
        return idP;
    }

    public void setIdP(int idP) {
        this.idP = idP;
    }

    public String getNomPrix() {
        return nomPrix;
    }

    public void setNomPrix(String nomPrix) {
        this.nomPrix = nomPrix;
    }

    public int getOrdre() {
        return ordre;
    }

    public void setOrdre(int ordre) {
        this.ordre = ordre;
    }

    public int idP;
    public String nomPrix;
    public int ordre;

    public Prix(int idP, String nomPrix, int ordre) {
        this.idP = idP;
        this.nomPrix = nomPrix;
        this.ordre = ordre;
    }
}
