package com.JDBC;

public class InfoProjet implements Comparable<InfoProjet>{
  public Notes not;
  public Projet proj;
  public Creneau cren;

  public InfoProjet(Notes n, Projet p, Creneau c){
    not=n;
    proj=p;
    cren=c;
  }


  @Override
  public int compareTo(InfoProjet infoProjet) {
    return cren.compareTo(infoProjet.cren);
  }
}
