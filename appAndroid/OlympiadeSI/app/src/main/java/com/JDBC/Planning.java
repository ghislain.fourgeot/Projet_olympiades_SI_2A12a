package com.JDBC;

public class Planning {
    private int idP;
    private int idJ;
    private int idC;

    public Planning(int idP, int idJ, int idC) {
        this.idP = idP;
        this.idJ = idJ;
        this.idC = idC;
    }

    public int getIdP() {
        return idP;
    }

    public void setIdP(int idP) {
        this.idP = idP;
    }

    public int getIdJ() {
        return idJ;
    }

    public void setIdJ(int idJ) {
        this.idJ = idJ;
    }

    public int getIdC() {
        return idC;
    }

    public void setIdC(int idC) {
        this.idC = idC;
    }
}
