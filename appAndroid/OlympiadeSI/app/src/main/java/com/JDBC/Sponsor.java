package com.JDBC;

public class Sponsor {
    private String nom;
    private String type;
    private String image;

    public Sponsor(String nom, String type, String image) {
        this.nom = nom;
        this.type = type;
        this.image = image;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
