package com.JDBC;

public class Personne{
	public int id;
	public String nom;
	public String prenom;

	public Personne(int i, String n, String p){
		this.id=i;
		this.nom=n;
    this.prenom=p;
	}

  public int getId(){
    return this.id;
  }

  public String getNom(){
    return this.nom;
  }

  public String getPrenom(){
    return this.prenom;
  }

  public void setId(int i){
    this.id=i;
  }

  public void setNom(String n){
    this.nom = n;
  }

  public void setPrenom(String p){
    this.prenom = p;
  }

}
