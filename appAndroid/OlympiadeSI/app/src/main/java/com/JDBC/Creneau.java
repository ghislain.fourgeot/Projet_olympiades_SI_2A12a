package com.JDBC;


public class Creneau implements Comparable<Creneau> {
    public int idC;
    public String hDebut;
    public String hFin;

    public Creneau(int idC, String hDebut, String hFin) {
        this.idC = idC;
        this.hDebut = hDebut;
        this.hFin = hFin;
    }


    /**
     * Fonction de comparaison de crenneau
     * @param creneau
     * @return
     */
    @Override
    public int compareTo(Creneau creneau) {
        String heuresthis = hDebut.substring(0,2);
        String heurecreneau = creneau.hDebut.substring(0,2);

        String minutethis = hDebut.substring(3,5);
        System.out.println(minutethis);
        String minutecrenneau = creneau.hDebut.substring(3,5);

        int heureThis = Integer.parseInt(heuresthis);
        int heureCrenneau = Integer.parseInt(heurecreneau);

        int minuteThis = Integer.parseInt(minutethis);
        int minuteCrenneau = Integer.parseInt(minutecrenneau);

        return heureThis == heureCrenneau ? Integer.compare(minuteThis, minuteCrenneau) : Integer.compare(heureThis,heureCrenneau);
    }
}
