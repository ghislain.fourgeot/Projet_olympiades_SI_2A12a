package com.JDBC;

public class Projet {
    public String idP;
    public String nomPro;
    public String description;
    public String image;
    public String emplacement;

    public Projet(String idP, String nomPro, String description, String image, String emplacement) {
        this.idP = idP;
        this.nomPro = nomPro;
        this.description = description;
        this.image = image;
        this.emplacement=emplacement;
    }

    public String getIdP() {
        return idP;
    }

    public void setIdP(String idP) {
        this.idP = idP;
    }

    public String getNomPro() {
        return nomPro;
    }

    public void setNomPro(String nomPro) {
        this.nomPro = nomPro;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getEmplacement() {
        return emplacement;
    }
}
